<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
     /**
     * The attributes that are mass assignable.s
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nombre','apellido1','apellido2','cedula','email','telefono','ipcliente','finger_id', 'direccion'
       ];
    protected $table = 'cliente';
    
    //A uno a finger
    public function Finger()
    {
        return $this->hasOne(Finger::class);
    }
}
