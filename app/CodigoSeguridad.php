<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CodigoSeguridad extends Model
{
     /**
     * The attributes that are mass assignable.s
     *
     * @var array
     */
    protected $fillable = [
        'id', 'codigo','estado','created_at'
       ];

      protected $table = 'codigo_seguridad';
      
        //A uno a estados
        public function EstadoConfirmacion()
        {
            return $this->hasOne(EstadoConfirmacion::class);
        }
}
