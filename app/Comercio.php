<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comercio extends Model
{
     /**
     * The attributes that are mass assignable.s
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nombre','telefono','correo','direccion','codVenta','tipoComercio_id'
       ];

    protected $table = 'comercio';

    //A uno a estados
    public function tipoComercio()
    {
        return $this->hasOne(TipoComercio::class);
    }
     //A uno a estados
     public function Producto()
     {
         return $this->hasMany(Producto::class);
     }
}
