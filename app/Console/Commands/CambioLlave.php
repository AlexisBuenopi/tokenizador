<?php

namespace App\Console\Commands;


use App\Mail\codeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Console\Command;
use App\Http\Clases\ConsumoApiConfig;
use App\Http\Clases\LogToken;
use App\Http\Clases\Tokens;
use App\Http\DAO\TarjetasDAO;
use App\Http\DAO\TokensDao;
use App\Http\DAO\LlaveUsuarioDao;
use Illuminate\Support\Facades\Log;
class CambioLlave  extends Command
{


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CronCambiollave:CambiarLlave';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando debe cambiar la llave cada 90 dias ';



    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->tarjetasDao = new TarjetasDao();
        $this->tokens = new Tokens();
        $this->tokenDao = new TokensDao();
        $this->llaveUsuarioDao = new LlaveUsuarioDao();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $existeDek = $this->llaveUsuarioDao->Getllave();
        $parameterkey =  $existeDek->parameterkey;
         $llave = ConsumoApiConfig::ConsumoServicios($parameterkey);
        // $llave = env('LLAVE', 'pruballavekey');

        try {
            LogToken::CrearLogJob(
                "Job Cambio de llave tokenizador",
                "Inicio job",
                "Inicio",
                "App/Console/Commands/CambioLlave.php" );

            $tarjetasList = $this->tarjetasDao->GetTarjetas();

            $ldate = date('Y-m-d H:i:s');
            $NuevaLlave = "KeyPagosInteligentes". $ldate;

            LogToken::CrearLogJob(
                "Job Cambio de llave tokenizador",
                "Consulto todas las tarjetas job",
                "Proceso",
                "App/Console/Commands/CambioLlave.php" );


            foreach ($tarjetasList as $key => $value) {

                LogToken::CrearLogJob(
                    "Job Cambio de llave tokenizador",
                    "Cambiando llave a tarjeta id ".$value->tarjeta,
                    "Inicio",
                    "App/Console/Commands/CambioLlave.php" );
                $tokenDescifrado = $this->tokens->DescrifrarToken($value->PanEncrypt, $llave);
                $newPanEncript= $this->tokens->creartoken($tokenDescifrado, $NuevaLlave);
                $this->tokenDao->ActulizarPanEncryptToken($newPanEncript,$value->token);
                $this->tarjetasDao->UpdateFechaTarjeta($value->tarjeta);
                LogToken::CrearLogJob(
                    "Job Cambio de llave tokenizador",
                    "Se cambio con exito la llave a la tarjeta id ".$value->tarjeta ,
                    "Exito",
                    "App/Console/Commands/CambioLlave.php" );
            }
            LogToken::CrearLogJob(
                "Job Cambio de llave tokenizador",
                "Enviando datos al servicio de juan",
                "En proceso",
                "App/Console/Commands/CambioLlave.php");

          $kms = ConsumoApiConfig::ConsumoServicios("KMS");
        //    $kms=env('KMS', 'dfdsfsdgbdfty5464b5674v6rv6t3645ujikygu');
            $consumoservicio = ConsumoApiConfig::ConsumoEnvioParamatros($NuevaLlave, "", $kms , 1);
            if ($consumoservicio == 200) {
                $this->llaveUsuarioDao->EliminarLlaveCifrado($llave);
                $this->llaveUsuarioDao->CrearLlaveCifrado($NuevaLlave, 1, 0);

                LogToken::CrearLogJob(
                    "Job Cambio de llave tokenizador",
                    "Fin job",
                    "Exito",
                    "App/Console/Commands/CambioLlave.php");
            }else{

                $this->llaveUsuarioDao->ActualizarLlaveCifrado($consumoservicio,1,0);
                $this->llaveUsuarioDao->CrearLlaveCifrado($NuevaLlave,0,1);
                LogToken::CrearLogJob(
                    "Job Cambio de llave tokenizador",
                    "Error al consumir servicios AWS - Servicios juan",
                    "Error",
                    "App/Console/Commands/CambioLlave.php");
            }
        } catch (\Throwable $th) {

            LogToken::CrearLogJob(
                "Job Cambio de llave tokenizador",
                "Error en el job",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile(),
                "App/Console/Commands/CambioLlave.php",
            );

            $msg = "Error en el job que cambia la llave tokenizadora";
            $email =  env('CORREOEMERGENCIA', 'leonardo.beltran.pi@gmail.com');
            Mail::to($email)->send(new codeMail($msg));
        }
    }
}
