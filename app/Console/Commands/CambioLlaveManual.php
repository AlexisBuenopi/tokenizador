<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\DAO\TarjetasDAO;
use App\Http\DAO\TokensDao;
use App\Http\DAO\LlaveUsuarioDao;
use App\Http\Clases\Tokens;
use App\Http\Clases\ConsumoApiConfig;
use App\Mail\codeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
class CambioLlaveManual extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CronCambioManual:cambioManual';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->tarjetasDao = new TarjetasDao();
        $this->tokens = new Tokens();
        $this->tokenDao = new TokensDao();
        $this->llaveUsuarioDao = new LlaveUsuarioDao();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
            $existeDek = $this->llaveUsuarioDao->Getllave();
            $parameterkey =  $existeDek->parameterkey;
             $llave = ConsumoApiConfig::ConsumoServicios($parameterkey);
            // $llave = env('LLAVE', 'pruballavekey');
            Log::debug("entro ");
            $fechaactual= Carbon::now()->subDays(30)->format('Y-m-d');
            Log::debug("fechaactual ".$fechaactual);
            try{

                $tarjetasList = $this->tarjetasDao->GetTarjetasNoActulizadas();
                $NuevaLlave = $this->llaveUsuarioDao->BuscarLlaveNueva();
                Log::debug("consumoservicio ".json_encode( $tarjetasList));
                foreach ($tarjetasList as $key => $value) {


                    $tokenDescifrado = $this->tokens->DescrifrarToken($value->PanEncrypt, $llave);
                    $newPanEncript= $this->tokens->creartoken($tokenDescifrado, $NuevaLlave);
                    $this->tokenDao->ActulizarPanEncryptToken($newPanEncript,$value->token);
                    $this->tarjetasDao->UpdateFechaTarjeta($value->tarjeta);

                }
                $kms = ConsumoApiConfig::ConsumoServicios("KMS");
                // $kms=env('KMS', 'dfdsfsdgbdfty5464b5674v6rv6t3645ujikygu');
                $consumoservicio = ConsumoApiConfig::ConsumoEnvioParamatros($NuevaLlave, "", $kms , 1);
                    if ($consumoservicio == 200) {
                        $this->llaveUsuarioDao->EliminarLlaveCifrado($llave);
                        $this->llaveUsuarioDao->CrearLlaveCifrado($NuevaLlave, 1, 0);

                    }else{

                        $this->llaveUsuarioDao->ActualizarLlaveCifrado($consumoservicio,1,0);
                        $this->llaveUsuarioDao->CrearLlaveCifrado($NuevaLlave,0,1);

                    }

            }catch (\Exception $e) {

            }

    }
}
