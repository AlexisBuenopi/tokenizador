<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
class DesactivarUsuaruiosInactivos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generator:Validar_Ultimo_Ingreso_User';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando debe validar los usuarios que llevan mas de 90 dias sin iniciar sesion';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $fecha = date('Y-m-d');

        $nuevafecha =  date(strtotime ( "-90 days" , strtotime ($fecha)));
        $nuevafecha =  date ( 'Y-m-d' , $nuevafecha );
        Log::debug("Fecha limite de ingreso".$nuevafecha );
        $usuarios = DB::table('users')->whereDate( 'fecha_Ingreso','<=',$nuevafecha )
        ->where('Estado','!=','0')
        ->select('id')->get();
        $NumeroUsuarios = DB::table('users')->whereDate( 'fecha_Ingreso','<=',$nuevafecha )
        ->where('Estado','!=','0')
        ->select('id')->count();
        Log::debug("Consulto los usuarios que tienen mas de 90 dias sin ingresar al usaurio" );
        Log::debug("Numero de usuarios a cambiar ".$NumeroUsuarios );

        foreach ($usuarios as $key => $value) {

            DB::table('users')->where('id', $value->id)
             ->update(['Estado' =>0]);
             Log::debug("Cambio el usuario ".$value->id);

            }
         Log::debug("------------------------------------------------------------------------" );

    }
}
