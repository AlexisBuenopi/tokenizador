<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Tarjeta;
use Carbon\Carbon;
use Dotenv\Dotenv;
class PaymentsCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:card';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enviar datos de tarjetas cada determinado tiempo, segun fecha estipulada';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $dotenv =  Dotenv::createImmutable ( __DIR__.'/../../../' ); 
        $dotenv ->load();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dateNow =  Carbon::now();
        $totalTarjetas = Tarjeta::leftJoin('token', 'token.id', '=', 'tarjeta.token_id')
        ->select('tarjeta.numTarjeta','tarjeta.cvv','tarjeta.id','token.fechaPagos','tarjeta.suscripcion_id','tarjeta.deleted_at')
                                           ->where('tarjeta.suscripcion_id',2)
                                           ->where('tarjeta.deleted_at',null)
                                           ->whereDay('token.fechaPagos', $dateNow->day)
                                           ->whereYear('token.fechaPagos', $dateNow->year)
                                           ->get();
        echo json_encode($totalTarjetas->toArray());                             
    }
}
