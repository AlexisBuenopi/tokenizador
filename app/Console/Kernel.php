<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use PHPUnit\Extensions\Selenium2TestCase\SessionCommand\Keys;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\PaymentsCron',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('payment:card')->everyMinute();
        // $schedule->command('generator:Validar_Ultimo_Ingreso_User')->everyMinute();
        $schedule->command('CronCambiollave:CambiarLlave')->quarterly()->between('1:00', '3:00');
       // $schedule->command('CronCambioManual:cambioManual')->everyMinute();

        // daily() cada dia
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
