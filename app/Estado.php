<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
      /**
     * The attributes that are mass assignable.s
     *
     * @var array
     */
    protected $fillable = [
        'id', 'descripcion','estado'
       ];

    protected $table = 'estado';

    //A uno a tarjeta
    public function tarjeta()
    {
        return $this->hasMany(tarjeta::class);
    }
}
