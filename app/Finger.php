<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Finger extends Model
{
     /**
     * The attributes that are mass assignable.s
     *
     * @var array
     */
    protected $fillable = [
        'id', 'user_agent','language','color_depth','device_memory','hardware_concurrency',
        'resolution','available_resolution','timezone_offset','session_storage','timezone',
        'local_storage','indexed_db','open_database','cpu_class','navigator_platform','regular_plugins',
        'canvas','webg1','web1_vendor','adblock','has_lied_languages','has_lied_resolution','has_lied_os',
        'has_lied_browser','touch_support','js_fonts','audio_bfb'
       ];
      protected $table = 'finger';
      
        //A uno a finger
        public function Cliente()
        {
            return $this->hasMany(Cliente::class);
        }
}
