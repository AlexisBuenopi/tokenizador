<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Franquicia extends Model
{
     /**
     * The attributes that are mass assignable.s
     *
     * @var array
     */
    protected $fillable = [
        'id', 'franquicia'
       ];
    protected $table = 'franquicia';

    //A uno a finger
    public function Tarjeta()
    {
        return $this->hasMany(Tarjeta::class);
    }

    
}
