<?php

namespace App\Http\Clases;
use Illuminate\Support\Facades\Log;


class ConsumoApiConfig
{

//Este metodo trae los valores que estan en el parameter store de amazon
    public static function ConsumoServicios($parameter){
    try {
            $arrContextOptions=array("ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
                ),
            'http'=>array(
                    'method'=>"GET",
                    'header'=>  'AuthKey:vi0L9nbAksPAXX1iKpOybYZzVXDtNJAiBsWMRjKRRHk='
                )

            );
            $url = "https://tokenizador.pagosinteligentes.com/parameter/".$parameter."?encrypted=true";
            $data =  file_get_contents($url, true ,stream_context_create($arrContextOptions));
            $result = \json_decode($data, true);
            return  $result[$parameter];

        } catch (\Exception $th) {
            Log::debug($th->getMessage().
            " \n La excepción se creó en la línea: " . $th->getLine().
            " \n El código de excepción es: " . $th->getCode().
            " \n En el archivo ".$th->getFile());
            //throw $th;
        }
    }

    //metodo que crear  la llave / datos parameter store
    public static function ConsumoEnvioParamatros($key, $key_value, $kms, $dek){

        try {

        $datosCodificados ="";
            //valores aleatorios
        if($dek==1){
            $datosCodificados = "[{\"is_dek\": true,
                \"kms_key\":\"".$kms."\",
                \"parameter_key\":\"".$key."\"
                }  ]";

        }else{
            //valores definidos por el usuario
            $datosCodificados = "[{\"is_dek\": false,
                \"kms_key\":\"".$kms."\",
                \"parameter_key\":\"".$key."\",
                \"parameter_value\":\"".$key_value."\"
                }  ]";
        }


       // $url = env('GETJUAN');
         $url = "https://tokenizadorqanlb-9382c3a9ad93d6df.elb.us-east-1.amazonaws.com:8096/parameter";
        $certificate =  "C:\xampp\cacert.pem";
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, false );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($ch, CURLOPT_CAINFO, $certificate);
        curl_setopt($ch, CURLOPT_CAPATH, $certificate);
        curl_setopt($ch,CURLOPT_POST, 1 );
        curl_setopt($ch,CURLOPT_URL, $url );

        curl_setopt($ch,CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json' ,
             'AuthKey:vi0L9nbAksPAXX1iKpOybYZzVXDtNJAiBsWMRjKRRHk='
        ) );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datosCodificados);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );

       $resultado = curl_exec($ch);

        $codigoRespberlisuesta = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $respuestaDecodificada = json_decode($resultado);
        $errmsg  = curl_error($ch);
        $err = curl_errno($ch);

        return $codigoRespberlisuesta;
    } catch (\Exception $th) {
        Log::debug($th->getMessage().
        " \n La excepción se creó en la línea: " . $th->getLine().
        " \n El código de excepción es: " . $th->getCode().
        " \n En el archivo ".$th->getFile());
    }
    }


    public static function ConsumoActualizacionParametros($key, $key_value, $kms){
try {
    //code...

        $datosCodificados ="";

        if($key_value==""){
            $datosCodificados = "[{
                \"kms_key\":\"".$kms."\",
                \"parameter_key\":\"".$key."\"
                }]";

        }else{
            $datosCodificados = "[{
                \"kms_key\":\"".$kms."\",
                \"parameter_key\":\"".$key."\",
                \"parameter_value\":\"".$key_value."\"
                }  ]";
        }



        $url = "https://tokenizadorqanlb-9382c3a9ad93d6df.elb.us-east-1.amazonaws.com:8097/parameter";
        $ch = curl_init();

        $certificate =  "C:\xampp\cacert.pem";
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, false );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($ch, CURLOPT_CAINFO, $certificate);
        curl_setopt($ch, CURLOPT_CAPATH, $certificate);
        curl_setopt($ch,CURLOPT_CUSTOMREQUEST , "PUT" );
        curl_setopt($ch,CURLOPT_URL, $url );
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt($ch,CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json' ,
            'AuthKey:vi0L9nbAksPAXX1iKpOybYZzVXDtNJAiBsWMRjKRRHk='
        ) );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $datosCodificados);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true );

       $resultado = curl_exec($ch);

        $codigoRespuesta = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $respuestaDecodificada = json_decode($resultado);
        $errmsg  = curl_error($ch);
        $err = curl_errno($ch);
        curl_close($ch);
        return $codigoRespuesta;
    } catch (\Exception $th) {
        Log::debug($th->getMessage().
        " \n La excepción se creó en la línea: " . $th->getLine().
        " \n El código de excepción es: " . $th->getCode().
        " \n En el archivo ".$th->getFile());
    }
    }



}
