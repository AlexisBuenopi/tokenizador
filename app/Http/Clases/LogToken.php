<?php
namespace App\Http\Clases;
use Illuminate\Support\Facades\Log;
class LogToken{


    public static function CrearLog($usuario, $evento, $resultado, $ruta, $tabla, $campos){

        $fecha = date('Y-m-d');
        $fichero = "Tokenizador_PagosInteligentes_".$fecha;
        $fp = fopen("./Logs/".$fichero.".txt", "a");

        $fechaconhora = date('Y-m-d h:i:s');
        fputs($fp,"\n");
        fputs($fp,"------------------------------------ $fechaconhora---------------------------------- "."\n");
        fputs($fp,"------------------------Tipo de Evento - ".$evento."--------------------------------\n");
        fputs($fp,"Identificacion Usuario - ".$usuario."\n");
        fputs($fp,"Resultado - ".$resultado."\n");
        fputs($fp,"Ruta - ".$ruta."\n");
        fputs($fp,"Tabla - ".$tabla."\n");
        fputs($fp,"Campos"."\n");
        fputs($fp,$campos."\n");
        fputs($fp,"---------------------------------------------------------------------------------- "."\n");
        fclose($fp);
    }

    public static function CrearLogJob($usuario, $evento, $resultado, $ruta){

        $fecha = date('Y-m-d');
        $fichero = "Cron_".$fecha;
        $fp = fopen("./public/LogsJob/".$fichero.".txt", "a");

        $fechaconhora = date('Y-m-d h:i:s');
        fputs($fp,"\n");
        fputs($fp,"------------------------------------ $fechaconhora---------------------------------- "."\n");
        fputs($fp,"------------------------Tipo de Evento - ".$evento."--------------------------------\n");
        fputs($fp,"Usuario - ".$usuario."\n");
        fputs($fp,"Resultado - ".$resultado."\n");
        fputs($fp,"Ruta - ".$ruta."\n");
        fputs($fp,"---------------------------------------------------------------------------------- "."\n");
        fclose($fp);
    }

















}

