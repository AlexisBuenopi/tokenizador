<?php
namespace App\Http\Clases;
use Illuminate\Support\Facades\Log;


class Tokens {

    public function creartoken($value, $key){

        try {
            $datos = [];
            //AES-256-ECB
            $method = "AES-256-CTR";
            $iv_length = openssl_cipher_iv_length($method);  //
            $iv = openssl_random_pseudo_bytes($iv_length);  //generar un numero byte de 16 digitos
            $first_encrypted  = openssl_encrypt($value,$method, $key, OPENSSL_RAW_DATA, $iv );
            $pancifrado = base64_encode($iv.$first_encrypted);
            $cadenaT = substr($value, -4);
            $cadena = $this->CadenaAleatoria();
            $NuevoToken = $cadena.$cadenaT;
            $datos[0] =  $pancifrado;
            $datos[1] =  $NuevoToken;



            return $datos;

        } catch (\Exception $th) {
            Log::debug("Error ".$th->getMessage().
            " \n La excepción se creó en la línea: " . $th->getLine().
            " \n El código de excepción es: " . $th->getCode().
            " \n En el archivo ".$th->getFile());
        }

    }

    public function DescrifrarToken($Token, $key){
        try {

            $mix =   base64_decode($Token);
            $method = "AES-256-CTR";
            $iv_length = openssl_cipher_iv_length($method);
            $iv = substr($mix,0,$iv_length);
            $first_encrypted = substr($mix,$iv_length);
            $data = openssl_decrypt($first_encrypted,$method,$key,OPENSSL_RAW_DATA,$iv);
            return $data;

        } catch (\Exception $th) {
            Log::debug("Error ".$th->getMessage().
            " \n La excepción se creó en la línea: " . $th->getLine().
            " \n El código de excepción es: " . $th->getCode().
            " \n En el archivo ".$th->getFile());
        }


    }

    public function CadenaAleatoria(){

         $lenght = 16;
         if (function_exists("random_bytes")) {
           $bytes = random_bytes(ceil($lenght / 2));
         } elseif (function_exists("openssl_random_pseudo_bytes")) {
         $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
         } else {
         }
         $cadena = substr(bin2hex($bytes), 0, $lenght);
         return $cadena;
    }



 }







