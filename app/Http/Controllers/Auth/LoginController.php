<?php

namespace App\Http\Controllers\Auth;

use App\Http\Clases\LogToken;
use App\CodigoSeguridad;
use App\Http\Controllers\Controller;
use App\Mail\codeMail;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\historialcontrasena;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Auth\Exception;
use App\Http\Clases\ConsumoApiConfig;

class LoginController extends Controller

{
    protected $maxAttempts = 5;
    protected $decayMinutes = 30;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {

        try {
            //code...

            $email = $request->email;
            //$this->validateLogin($request);
            $validacionuser = false;
            LogToken::CrearLog(
                $email,
                "Intento de Ingresos al Sistema Tokenizador",
                "En proceso",
                "Http/Controller/LoginController.php",
                "User",
                "User",
                "",
                ""
            );

            $credentials = $request->only('email', 'password');

            if (Auth::attempt($credentials)) {
                $validacionuser = true;
            }

            if ($validacionuser) {

                if (
                    method_exists($this, 'hasTooManyLoginAttempts') &&
                    $this->hasTooManyLoginAttempts($request)
                ) {

                    $this->fireLockoutEvent($request);

                    return $this->sendLockoutResponse($request);
                }
                $Idusuario = DB::table('users')
                    ->where('email', $request->email)
                    ->select('id', 'email', 'name', 'password', 'rol_id')->first();


                if (!is_null($Idusuario)) {

                    if (Hash::check($request->password, $Idusuario->password)) {
                        LogToken::CrearLog(
                            $email,
                            "Validar contraseña",
                            "En Proceso",
                            "Http/Controller/LoginController.php",
                            "historialcontrasena",
                            ""
                        );

                        $passscorrecto = $this->ValidarContrasenas($request->password, $Idusuario->id);

                        if ($passscorrecto) {
                            LogToken::CrearLog(
                                $email,
                                "Validacion de  contraseña",
                                "Exito",
                                "Http/Controller/LoginController.php",
                                "historialcontrasena",
                                ""
                            );

                            $code = $request->get('code');
                            LogToken::CrearLog(
                                $email,
                                "Validar Estado del Usuario",
                                "En Proceso",
                                "Http/Controller/LoginController.php",
                                "users",
                                ""
                            );

                            $estadoUsuario = $this->validarEstadoUsuario($Idusuario->id);
                            // si el codigo viene, validarlo

                            if ($estadoUsuario) {
                                LogToken::CrearLog(
                                    $email,
                                    "Validacion Estado del Usuario",
                                    "Exito",
                                    "Http/Controller/LoginController.php",
                                    "users",
                                    ""
                                );

                                if ($code) {
                                    if ($this->validateSentCode($code, $request)) {
                                        LogToken::CrearLog(
                                            $email,
                                            "Validacion del Codigo",
                                            "Exitoso",
                                            "Http/Controller/LoginController.php",
                                            "codigo_seguridad",
                                            ""
                                        );

                                        // ingresa a hacer login e iniciar sesion≈
                                        if ($this->attemptLogin($request)) {
                                            LogToken::CrearLog(
                                                $Idusuario->id,
                                                "Inicio de sesion",
                                                "Exitoso",
                                                "Http/Controller/LoginController.php",
                                                "users",
                                                ""
                                            );

                                            session_start();
                                            $_SESSION["idusuario"] =  $Idusuario->id;
                                            $_SESSION["rol_id"] =  $Idusuario->rol_id;
                                            $iduser = $_SESSION["idusuario"];

                                            DB::table('users')->where('id', $Idusuario->id)
                                                ->update([
                                                    'Fecha_Ingreso' => date("Y-m-d H:i:s")
                                                ]);
                                            return $this->sendLoginResponse($request);
                                        }else{

                                            LogToken::CrearLog(
                                                $Idusuario->id,
                                                "Envio del codigo",
                                                "Fallido",
                                                "Http/Controller/LoginController.php",
                                                "users",
                                                ""
                                            );

                                            $email = $request->email;
                                            $password = $request->password;
                                            $showCode = false;
                                            $codeInvalid = true;
                                            $msg = false;
                                            return view('auth.login', compact('codeInvalid', 'email', 'code', 'showCode', 'password', 'msg'));

                                        }


                                    }
                                } else {
                                    if ($this->guard()->once($this->credentials($request))) {
                                        $this->guard()->logout();
                                        $hoy = date("Y-m-d H:i:s");
                                        LogToken::CrearLog(
                                            $code,
                                            "Intento Ingreso",
                                            "valida",
                                            "Http/Controller/LoginController.php",
                                            "User",
                                            ""
                                        );
                                        // el usuario existe y enviar correo con codigo
                                        if ($this->sendCodeToEmail($request)) {
                                            $email = $request->email;
                                            $password = $request->password;
                                            $showCode = true;
                                            $msg = false;
                                            LogToken::CrearLog(
                                                $code,
                                                "Envio del Codigo",
                                                "Exitoso",
                                                "Http/Controller/LoginController.php",
                                                "User",
                                                ""
                                            );

                                            // redireccionar a login para que muestre el campo de codigo
                                            return view('auth.login', compact('showCode', 'email', 'password', 'msg'));
                                        } else {
                                            LogToken::CrearLog(
                                                $Idusuario->id,
                                                "Envio de codigo",
                                                "Fallido",
                                                "Http/Controller/Auth/LoginController.php",
                                                "users",
                                                ""
                                            );

                                            $email = $request->email;
                                            $password = $request->password;
                                            $msg = true;
                                            $showCode = false;
                                            // redireccionar a login para que muestre el campo de codigo
                                            return view('auth.login', compact('showCode', 'email', 'password', 'msg'));
                                        }
                                    }
                                }
                            } else {

                                LogToken::CrearLog(
                                    $Idusuario->id,
                                    "Validar Estado del Usuario",
                                    "Fallido",
                                    "Http/Controller/LoginController.php",
                                    "User",
                                    "User",
                                    "",
                                    ""
                                );

                                Auth::logout();
                                $email = $request->email;
                                $password = $request->password;
                                $msg = false;
                                $showCode = false;
                                $mensaje = [
                                    'email' => 'El usuario se encuentra desactivado, por favor pongase en contacto con el administrador',
                                ];
                                return view('auth.login', compact('showCode', 'email', 'password', 'msg'))->withErrors($mensaje);
                            }

                            // If the el login o codigo fue incorrecto incrementar los intentos de login y
                            //  mostrar la pantalla de fallo
                            $this->incrementLoginAttempts($request);
                            return $this->sendFailedLoginResponse($request);
                        } else {
                            LogToken::CrearLog(
                                $request->email,
                                "Validacion de contraseña",
                                "Fallido",
                                "Http/Controller/LoginController.php",
                                "historialcontrasena",
                                "User",
                                ""
                            );

                            Auth::logout();
                            $usuario = $Idusuario->id;
                            return  view('auth.passwords.reset2', compact('usuario'));
                        }
                    }
                } else {
                    Auth::logout();
                    $email = $request->email;
                    $password = $request->password;
                    $msg = false;
                    $showCode = false;
                    $mensaje = [
                        'password' => 'La contraseña no coincide',
                    ];
                    return view('auth.login', compact('showCode', 'email', 'password', 'msg'))->withErrors($mensaje);
                }
            } else {
                LogToken::CrearLog(
                    $email,
                    "Intento de Ingresos al Sistema Tokenizador",
                    "Fallido",
                    "Http/Controller/LoginController.php",
                    "User",
                    ""
                );

                Auth::logout();
                $email = $request->email;
                $password = $request->password;
                $msg = false;
                $showCode = false;
                $mensaje = [
                    'password' => 'La contraseña no coincide',
                ];
                return view('auth.login', compact('showCode', 'email', 'password', 'msg'))->withErrors($mensaje);
            }
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $request->email,
                "Error",
                "Error",
                "Http/Controller/Auth/LoginController.php",
                "login",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
        }
    }


    public function ValidarContrasenas($contraseña, $idusuario)
    {
        $fechacontrasena = "";
        $validarfecha = false;

        $NumeroPasswords = DB::table('historialcontrasena')->where('idusuario', $idusuario)->count();

        if ($NumeroPasswords > 1) {
            $Paasssword = DB::table('historialcontrasena')->where('idusuario', $idusuario)->select('password', 'created_at')->orderBy('created_at', 'desc')->first();
            $dias =     DB::table('dias')->select('dias')->first();

            if (Hash::check($contraseña, $Paasssword->password)) {
                //capturo la fecha de creacion de la contraseña
                $fechacontrasena = $Paasssword->created_at;
            }

            $fecha = date('Y-m-d');
            $fechacom = "-" . $dias->dias . " day";
            $nuevafecha =  date(strtotime($fechacom, strtotime($fecha)));
            $nuevafecha =  date('Y-m-d', $nuevafecha);
            $fechacontrasena = date('Y-m-d', strtotime($fechacontrasena));


            if ($nuevafecha <= $fechacontrasena) {
                $validarfecha = true;
            } else {
                $validarfecha = false;
            }
        } else {
            $validarfecha = false;
        }





        return $validarfecha;
    }

    function token()
    {
        $lenght = 10;

        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($lenght / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }

        $token = substr(bin2hex($bytes), 0, $lenght);
        return $token;
    }

    private function sendCodeToEmail($request)
    {

        try {
            $idUser = User::select('id', 'codigo_seguridad_id')->where('email', $request->get('email'))->first();
            $codeSend = CodigoSeguridad::select('created_at')->where('id',  $idUser->codigo_seguridad_id)->first();
            // $dateCode = new Carbon($codeSend->created_at);
            $dateCode =  Carbon::now();
            $dateNow = Carbon::now();
            $difference = $dateNow->diffInMinutes($dateCode);
            // $msg = random_int(100000, 999999);
            $msg = $this->token();
            $passencrypt = Hash::make($msg);
            //guardar codigo en base de datos y asignarle un estado
            $codigo = new   CodigoSeguridad;
            $codigo->estado = 1;
            $codigo->code = $passencrypt;
            $codigo->save();
            // dd($idUser);
            User::where('id', $idUser->id)->update(['codigo_seguridad_id' => $codigo->id]);
            $email = $request->get('email');
            LogToken::CrearLog(
                $request->get('email'),
                "Envio de  correo",
                "Envio de correo con token de verificacion",
                "Http/Controller/Auth/LoginController.php",
                "",
               "Enviando correo a: ".$email
            );


            Mail::to($email)->send(new codeMail($msg));
            return true;
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $request->get('email'),
                "Error",
                "Error",
                "Http/Controller/Auth/LoginController.php",
                "BB",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
            return false;
        }
    }

    private function validateSentCode($code, $request)
    {

        $idForeingUser = User::select('codigo_seguridad_id')->where('email', $request->get('email'))->first();
        $codeSend = CodigoSeguridad::select('code', 'created_at')->where('id', $idForeingUser->codigo_seguridad_id)->first();
        //si tiene un intervalo de minutos mayor a 5 el codigo caduca inmediatamente
        $dateCode = new Carbon($codeSend->created_at);
        $dateNow = Carbon::now();
        $difference = $dateNow->diffInMinutes($dateCode);
        if ((Hash::check($code, $codeSend->code)) && $difference <= 5) {
            return true;
        } else {
            //cuando se valida caduca
            $codeSend = CodigoSeguridad::where('id', $idForeingUser->codigo_seguridad_id)->update(['estado' => 0]);
            return false;
        }
    }


    private function validarEstadoUsuario($idusuario)
    {
        $validarEstado = false;
        $user = DB::table('users')->where('id', $idusuario)->select('Estado')->first();
        if ($user->Estado == 1) {
            $validarEstado = true;
        }
        return $validarEstado;
    }


    public function Inicio()
    {
        return view('auth.login');
    }
}
