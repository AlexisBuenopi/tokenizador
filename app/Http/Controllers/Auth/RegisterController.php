<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Http\Clases\Tokens;
use App\Http\Clases\LogToken;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Mail\confirmMail;
use Illuminate\Support\Facades\DB;
use ILluminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        //dd($data);
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'rol' => ['required', 'string'],
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        //dd($request);
        $this->validator($request->all())->validate();
        Log::debug("Valido usuario");
        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);

        return $this->registered($request, $user)
                        ? redirect()->route('show-userregister') : redirect()->route('show-userregister');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data){
        //dd($data);
        try {
            DB::beginTransaction();
            session_start();
            $iduser =  $_SESSION["idusuario"];   
            $Usuario = $this->CrearUsuario($data, $iduser);           
            $this->CrearHistorialPassword($Usuario, $iduser);
            DB::commit();

        } catch (\Exception $th) {
            LogToken::CrearLog($iduser, "Error al crear usuario", "Error",
            "Http/Controller/RegisterController.php",
            "Users","Error ".$th->getMessage().
            " \n La excepción se creó en la línea: " . $th->getLine().
            " \n El código de excepción es: " . $th->getCode().
            " \n En el archivo ".$th->getFile());      
            DB::rollBack();
        }      
     
       
        return redirect()->route('show-userregister');
    }



    public function CrearUsuario(array $data, $iduser){
     
        try {
        
       
            $token = new Tokens();
            LogToken::CrearLog($iduser, "Creacion de contraseña", "En Proceso",
            "Http/Controller/RegisterController.php",
            "User","");
         
            $password = $token->CadenaAleatoria();
      
            LogToken::CrearLog($iduser, "Creacion de contraseña", "Exito",
            "Http/Controller/RegisterController.php",
            "Users",""); 
           
            $passencrypt = Hash::make($password);
            LogToken::CrearLog($iduser, "Encriptar Contraseña", "Exito",
            "Http/Controller/RegisterController.php",
            "User","");
         
           $rol = $data['rol']; 

           LogToken::CrearLog($iduser, "Crear Usuario", "Intento",
           "Http/Controller/RegisterController.php",
           "User","");
        
            $nuevoUsuario = User::create([      
                'name' => $data['name'],
                'email' => $data['email'],
                'password' =>$passencrypt,
                'cargo' => $data['cargo'],
                'Estado'=> 1,
                'created_at'=> now(),             
                'rol_id'=>$rol
            ]);
            LogToken::CrearLog($iduser, "Crear Usuario", "Exito",
            "Http/Controller/RegisterController.php",
            "User","name: ".$data['name']." email: ".$data['email'].
            " cargo: ".$data['cargo']." rol: ".$data['rol']); 
         
      
            $email = $data['email'];
            \Mail::to($email)->send(new confirmMail($email,$password));
            LogToken::CrearLog($iduser, "Envio de correo", "Exito",
            "Http/Controller/RegisterController.php",
            "User",$email);
          
            return $nuevoUsuario;
        } catch (\Exception $th) {
            LogToken::CrearLog($iduser, "Error al crear usuario", "Error",
            "Http/Controller/RegisterController.php",
            "Users","Error ".$th->getMessage().
            " \n La excepción se creó en la línea: " . $th->getLine().
            " \n El código de excepción es: " . $th->getCode().
            " \n En el archivo ".$th->getFile());            
        }


    }

    public function CrearHistorialPassword($usuario, $iduser){
        LogToken::CrearLog($iduser, "Crear Historial contraseña", "Intento",
        "Http/Controller/RegisterController.php",
        "historialcontrasena","idusuario: ".$usuario->id);


        $Idusuario = DB::table('users')->where('id',$usuario->id)->select('id', 'email', 'name', 'password')->first();
    
        DB::table('historialcontrasena')->insert(
            ['password' => $Idusuario->password,
            'estado' => 1,
            'fechaCreacion' => date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
            'idusuario' => $usuario->id                
        ]);
        LogToken::CrearLog($iduser, "Crear Historial contraseña", "Exito",
        "Http/Controller/RegisterController.php",
        "historialcontrasena","idusuario: ".$usuario->id." estado: 1"." fechaCreacion: ".date("Y-m-d H:i:s").
        " created_at: ".date("Y-m-d H:i:s")." updated_at: ".date("Y-m-d H:i:s"));



    }


}