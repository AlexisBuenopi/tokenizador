<?php

namespace App\Http\Controllers\Auth;
use App\Http\Clases\LogToken;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use App\historialcontrasena;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
      
        dd($notifiable);
        if (static::$toMailCallback) {
            Log::debug("Valido usuario");
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        if (static::$createUrlCallback) {
            Log::debug("");
            $url = call_user_func(static::$createUrlCallback, $notifiable, $this->token);
        } else {
            $url = url(route('password.reset', [
                'token' => $this->token,
                'email' => $notifiable->getEmailForPasswordReset(),
            ], false));
        }

        return (new MailMessage)
            ->subject('Reset Password noti')
            ->line(Lang::get('You are receiving this email because we received a password reset request for your account.'))
            ->action(Lang::get('Reset Password'), $url)
            ->line(Lang::get('This password reset link will expire in :count minutes.', ['count' => config('auth.passwords.'.config('auth.defaults.passwords').'.expire')]))
            ->line(Lang::get('If you did not request a password reset, no further action is required.'));
    }

    
    public function CambiarPass(Request $request){      
        $passantiguo = $request->password;  
        LogToken::CrearLog($request->usuario, "Validando contraseña antigua", "En Proceso",
        "Http/Controller/RegisterController.php",  "historialcontrasena","");      
        $comprobarPassAnt = $this->ValidarPassAntigua($request->usuario, $passantiguo);
     if ($comprobarPassAnt) {
        LogToken::CrearLog($request->usuario, "Validacion de contraseña antigua", "Exito",
        "Http/Controller/RegisterController.php",
        "historialcontrasena","");    
        $passnueva = $request->new_password;    
        LogToken::CrearLog($request->usuario, "Validacion de contraseña nueva", "En proceso",
        "Http/Controller/RegisterController.php",
        "historialcontrasena",""); 
        $passnumlet = $this->ValidarNumLetPassNew($passnueva);
        if( $passnumlet){

       
            $comprobarPassNew = $this->ValidarPassNueva($request->usuario, $passnueva);       
            $passencript = Hash::make($passnueva);
                if ($comprobarPassNew) {
                    LogToken::CrearLog($request->usuario, "Validacion de contraseña nueva", "Exito",
                    "Http/Controller/RegisterController.php",  "historialcontrasena","");  
                    DB::beginTransaction();
                    try {    
                        LogToken::CrearLog($request->usuario, "Modificando contrasena", "En proceso",
                        "Http/Controller/RegisterController.php", "users","");               
                        DB::table('users')->where('id', $request->usuario)
                ->update(['password' =>$passencript  ,
                'updated_at' => date("Y-m-d H:i:s")  
                    ]);    

                    LogToken::CrearLog($request->usuario, "Modificando contrasena", "Exito",
                    "Http/Controller/RegisterController.php", "users",""); 

                    LogToken::CrearLog($request->usuario, "Insertando contrasena nueva en  el historial", "En proceso",
                    "Http/Controller/RegisterController.php", "historialcontrasena","");     
                DB::table('historialcontrasena')->insert(
                    ['password' => $passencript ,
                    'estado' => 1,
                    'fechaCreacion' => date("Y-m-d H:i:s"),
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'idusuario' => $request->usuario                 
                    ]
                );             
                LogToken::CrearLog($request->usuario, "Insertando contrasena nueva en  el historial", "Exito",
                "Http/Controller/RegisterController.php", "historialcontrasena","");   
                        DB::commit();

                        return view('auth.login');
                    } catch (\Exception $th) {
                        DB::rollBack();
                        LogToken::CrearLog($request->usuario, "Error al crear usuario", "Error",
                        "Http/Controller/RegisterController.php",
                        "Users","Error ".$th->getMessage().
                        " \n La excepción se creó en la línea: " . $th->getLine().
                        " \n El código de excepción es: " . $th->getCode().
                        " \n En el archivo ".$th->getFile());  
                    }              
                
                }else{
                    $usuario = $request->usuario;
                    $mensaje =[
                        'new_password' => 'La contraseña nueva ya a sido registrada, por favor  intente con una nueva',
                    ];
                    return  view('auth.passwords.reset2', compact('usuario'))->withErrors($mensaje);   
        

            }
       }else{
        $usuario = $request->usuario;
        LogToken::CrearLog($request->usuario, "Validacion caracteres de la contraseña nueva", "Fallido",
        "Http/Controller/RegisterController.php",
        "historialcontrasena","");  
        $mensaje =[
            'new_password' => 'La contraseña nueva debe tener numeros y letras',
        ];
        return  view('auth.passwords.reset2', compact( 'usuario'))->withErrors($mensaje);   

       }
           
    }else{           
            $usuario = $request->usuario;
            LogToken::CrearLog($request->usuario, "Validacion de contraseña nueva", "Fallido",
            "Http/Controller/RegisterController.php",
            "historialcontrasena","");  
            $mensaje =[
                'password' => 'La contraseña anterior no coincide',
            ];
            return  view('auth.passwords.reset2', compact( 'usuario'))->withErrors($mensaje);   

        }
      
    }



    public function ValidarPassAntigua($usuario, $passnatiguo){

        $Idusuario = DB::table('users')->where('id',$usuario)->select('id', 'email', 'name', 'password')->first();
    
        if (!is_null($Idusuario)) {

            if(Hash::check($passnatiguo, $Idusuario->password)) {
                return true;
            }else{
                return false;
            }           
        }else{
            return false;
        }


    }


    public function ValidarPassNueva($usuario, $passNueva){
        $passcorrecto = true;
        $Idusuario = DB::table('historialcontrasena')->where('idusuario',$usuario)->select('id', 'password')->take(5)->orderby('created_at', 'desc')->get();
 
        if (!is_null($Idusuario )) {  
              
            foreach ($Idusuario as $key => $value) {  
              
                if(Hash::check($passNueva, $value->password)) {
                    $passcorrecto = false;
                break;
                } 
            }             
        }else{
            return false;
        }
    return $passcorrecto;
    }

    public function ValidarNumLetPassNew($passNueva){


        if (preg_match('([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z])', $passNueva)) {

            return true;
         } else {
    
            return false;
         }
    }
 
}
