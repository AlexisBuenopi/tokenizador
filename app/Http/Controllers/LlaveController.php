<?php

namespace App\Http\Controllers;

use App\LlaveUsuario;
use Illuminate\Http\Request;
use App\Http\Clases\LogToken;
use Illuminate\Support\Facades\Log;
use App\Http\Clases\ConsumoApiConfig;
use Illuminate\Support\Facades\DB;

class LlaveController extends Controller
{

    public function index(Request $request)
    {
        $buscarLlaves = DB::table('llaveusuario')->select('parameterkey', 'Estado', 'created_at', 'id', 'Esdek')
            ->get();

        return view('pages.buscarLlave', compact('buscarLlaves'));
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            LogToken::CrearLog(
                $_SESSION["idusuario"],
                "Creación de la llave de cifrado",
                "En proceso",
                "Http/Controller/TokenController.php",
                "llaveusuario",
                ""
            );

            $dek = $request->Esdek == 'on' ? 1 : 0;
            $Esdek = $request->Esdek == 'on' ? true : false;

            $Llaveuser =  DB::table('llaveusuario')->where('parameterkey', $request->key)
                ->select('id', 'parameterkey', 'Estado')->first();

            if (is_null($Llaveuser)) {
                $ComprobarDEK =  DB::table('llaveusuario')->where('Esdek', 1)
                    ->select('id', 'parameterkey', 'Estado')->first();


                if ($dek == 0 || is_null($ComprobarDEK)) {

                    $consumoservicio = ConsumoApiConfig::ConsumoEnvioParamatros($request->key, $request->key_value, $request->KMS_key, $dek);

                    if ($consumoservicio == 200) {

                        session_start();
                        $idusuario = $_SESSION["idusuario"];


                        LlaveUsuario::create(
                            [
                                'parameterkey' =>   $request->key,
                                'idusuario' => $idusuario,
                                'Estado' => False,
                                'created_at' => now(),
                                'updated_at' => now(),
                                'Esdek' => $Esdek
                            ]
                        );

                        LogToken::CrearLog(
                            $_SESSION["idusuario"],
                            "Creación de la llave de cifrado",
                            "Exitosd",
                            "Http/Controller/TokenController.php",
                            "llaveusuario",
                            ""
                        );

                        DB::commit();
                        return redirect()->route('home')->with('status', 'Llave generada correctamente');
                    } else if ($consumoservicio === 401) {
                        $mensaje = [
                            'key' => 'La llave ya esta creada',
                        ];
                        LogToken::CrearLog(
                            $_SESSION["idusuario"],
                            "Error en la Creación de la llave de cifrado",
                            "Error",
                            "Http/Controller/TokenController.php",
                            "llaveusuario",
                            "La llave ya esta creada"
                        );



                        return  view('dashboard')->withErrors($mensaje);
                    }
                } else {
                    $key =  $request->key;
                    $key_value =  $request->key_value;
                    $KMS_key =  $request->KMS_key;
                    $Esdek =  $request->Esdek;

                    $mensaje = [
                        'key' => 'No puede crear mas de una llave DEK',
                    ];
                    LogToken::CrearLog(
                        $_SESSION["idusuario"],
                        "Error en la Creación de la llave de cifrado",
                        "Error",
                        "Http/Controller/TokenController.php",
                        "llaveusuario",
                        "No puede crear mas de una llave DEK"
                    );

                    return  view('dashboard', compact('key', 'key_value', 'KMS_key', 'Esdek'))->withErrors($mensaje);
                }
            } else {
                $key =  $request->key;
                $key_value =  $request->key_value;
                $KMS_key =  $request->KMS_key;
                $Esdek =  $request->Esdek;

                $mensaje = [
                    'key' => 'La llave ya esta creada',
                ];
                LogToken::CrearLog(
                    $_SESSION["idusuario"],
                    "Error en la Creación de la llave de cifrado",
                    "Error",
                    "Http/Controller/TokenController.php",
                    "llaveusuario",
                    "La llave ya esta creada"
                );

                return  view('dashboard', compact('key', 'key_value', 'KMS_key', 'Esdek'))->withErrors($mensaje);
            }
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $_SESSION["idusuario"],
                "Error al creacion de la llave de cifrado",
                "Error",
                "Http/Controller/LlaveController.php",
                "llaveusuario",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );

            DB::rollBack();
        }
    }

    public function GetIdLlave(Request $request)
    {

        $Llave = DB::table('llaveusuario')
            ->where('id', $request->idllave)
            ->select('id', 'idusuario', 'parameterkey', 'Esdek')->first();

        return response()->json($Llave);
    }

    public function GetParamterKey(Request $request)
    {
        $parametros =  $request->parameterkey;
        $paramValues = ConsumoApiConfig::ConsumoServicios($parametros);
        return response()->json($paramValues);
    }

    public function Update(Request $request)
    {

        try {

            DB::beginTransaction();
            LogToken::CrearLog(
                $_SESSION["idusuario"],
                "Actualización de la llave de cifrado",
                "En proceso",
                "Http/Controller/LlaveController.php",
                "llaveusuario",
                ""
            );



            $consumoservicio = ConsumoApiConfig::ConsumoActualizacionParametros($request->key, $request->key_value, $request->KMS_key);
            if ($consumoservicio === 200) {
                LogToken::CrearLog(
                    $_SESSION["idusuario"],
                    "Actualización de la llave de cifrado",
                    "Exitoso",
                    "Http/Controller/LlaveController.php",
                    "llaveusuario",
                    ""
                );



                DB::table('llaveusuario')->where('parameterkey', $request->key)
                    ->update(['updated_at' => date("Y-m-d H:i:s")]);
            } else {
                LogToken::CrearLog(
                    $_SESSION["idusuario"],
                    "Error en la actualización de la llave de cifrado",
                    "Error",
                    "Http/Controller/LlaveController.php",
                    "llaveusuario",
                    ""
                );

            }
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $_SESSION["idusuario"],
                "Error al actualizar la llave de cifrado",
                "Error",
                "Http/Controller/LlaveController.php",
                "llaveusuario",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
            DB::rollBack();
        }
        return redirect()->route('listarllave')->with('status', 'Llave Actualizada correctamente');
    }

    public function UpdateEstadoLlave(Request $request)
    {

        $valorEstado = $request->Estado == 'true' ? 1 : 0;
        DB::table('llaveusuario')->where('id', $request->idparameter)
            ->update(['Estado' =>   $valorEstado]);
    }
}
