<?php

namespace App\Http\Controllers;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Clases\LogToken;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Auth\ResetPasswordController;
class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        return view('profile.edit');
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request)
    {
        auth()->user()->update($request->all());

        return back()->withStatus(__('Profile successfully updated.'));
    }

    /**
     * Change the password
     *  
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        LogToken::CrearLog(auth()->user()->id , "Validacion numeros y letras en contraseña", "En proceso",
        "Http/Controller/ProfileController.php", "historialcontrasena","");   
           

        $passantiguo = $request->get('password');
        $resetpass = new ResetPasswordController();

        $passnumlet = $resetpass->ValidarNumLetPassNew($passantiguo);
     
        if($passnumlet){
         $passencript =   Hash::make($passantiguo);
   
         LogToken::CrearLog(auth()->user()->id , "Validacion numeros y letras en contraseña", "Exito",
         "Http/Controller/ProfileController.php", "historialcontrasena","");   
         LogToken::CrearLog(auth()->user()->id , "Validar contraseña con el historial", "en proceso",
         "Http/Controller/ProfileController.php", "historialcontrasena",""); 
         $validarpassNueno =     $resetpass->ValidarPassNueva( auth()->user()->id ,$passantiguo);

                if (  $validarpassNueno) {
                    auth()->user()->update(['password' => $passencript]); 

                    LogToken::CrearLog(auth()->user()->id , "Validar contraseña con el historial", "Exito",
                    "Http/Controller/ProfileController.php", "historialcontrasena",""); 
                    DB::table('historialcontrasena')->insert(
                        ['password' =>   $passencript,
                        'estado' => 1,
                        'fechaCreacion' => date("Y-m-d H:i:s"),
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s"),
                        'idusuario' =>  auth()->user()->id                 
                        ]
                    );
                    LogToken::CrearLog(auth()->user()->id , "actualizar contraseña", "Exito",
                    "Http/Controller/ProfileController.php", "historialcontrasena",""); 
                    return back()->withStatusPassword(__('Password actualizado exitosamente.'));


                }else{
                    LogToken::CrearLog(auth()->user()->id , "Validar contraseña con el historial", "Fallido",
                    "Http/Controller/ProfileController.php", "historialcontrasena",""); 
                   $mensaje =[
                        'password' => 'La contraseña ya a sido utilizada',
                    ];
                    return back()->withErrors($mensaje);
                }
        
        }else{
            LogToken::CrearLog(auth()->user()->id , "Validacion numeros y letras en contraseña", "Fallido",
            "Http/Controller/ProfileController.php", "historialcontrasena",""); 
          
            $password = $request->get('password');
            $mensaje =[
                'password' => 'La contraseña debe  ser alfanumerica',
            ];
            return back()->withErrors($mensaje);
       
        }

    }
}
