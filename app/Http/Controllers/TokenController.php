<?php

namespace App\Http\Controllers;

use App\Http\Clases\LogToken;
use App\Comercio;
use App\Franquicia;
use App\Producto;
use App\Tarjeta;
use App\PagosMensualesComercio;
use App\Http\Clases\Tokens;
use App\Suscripcion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Clases\ConsumoApiConfig;
use App\Http\DAO\TarjetasDAO;
use App\Http\DAO\TokensDao;
use App\Http\DAO\TokenKeyDao;
use App\Http\DAO\ClienteDao;
use App\Http\DAO\LlaveUsuarioDao;
use App\Http\DAO\FranquiciaDao;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;


class TokenController extends Controller
{

 protected $tokenDao;
 protected $tokenKeyDao;
 protected $tarjetasDao;
 protected $clienteDao;
 protected $llaveUsuarioDao;
 protected $franquiciaDao;

    public function __CONSTRUCT(){
        $this->tokenDao = new TokensDao();
        $this->tokenKeyDao = new TokenKeyDao();
        $this->tarjetasDao = new TarjetasDao();
        $this->clienteDao = new ClienteDao();
        $this->llaveUsuarioDao = new LlaveUsuarioDao();
        $this->franquiciaDao = new FranquiciaDao();
    }

    public function index(Request $request)
    {
        return view('pages.creartoken');
    }

    public function creditCardReveal(Request $request)
    {
        $arrayCard = [
            "VISA" => "/^4\d{12}(\d{3})?$/",
            "MasterCard" => "/^(5[1-5]\d{4}|677189)\d{10}$/",
            "Discover" => "/^6(?:011\d\d|5\d{4}|4[4-9]\d{3}|22(?:1(?:2[6-9]|[3-9]\d)|[2-8]\d\d|9(?:[01]\d|2[0-5])))\d{10}$/",
            "Amex" => "/^3[47]\d{13}$/",
            "Diners" => "/^3(0[0-5]|[68]\d)\d{11}$/",
            "JCB" => "/^35(28|29|[3-8]\d)\d{12}$/",
            "Switch" => "/^6759\d{12}(\d{2,3})?$/",
            "Solo" => "/^6767\d{12}(\d{2,3})?$/",
            "Dankort" => "/^5019\d{12}$/",
            "Maestro" => "/^(5[06-8]|6\d)\d{10,17}$/",
            "Forbrugsforeningen" => "/^600722\d{10}$/",
            "Laser" => "/^(6304|6706|6771|6709)\d{8}(\d{4}|\d{6,7})?$/",
            "Unknown" => "/.*/",
        ];

        $numberCard = $request->number;
        foreach ($arrayCard as $clave => $valor) {
            if (preg_match($valor, $numberCard)) {
                $result = ['result' => $clave];
                return response()->json($result);
            }
        }
        // return response()->json('');
        // $clave =  $request->number;
        // $result = ['result' => $clave];
        return response()->json("");
    }
    /**
     * Valentina santa osorio
     * Funcion que permite realizar el filtro de un token ,por medio de su nombre
     * esta busqueda permite gestionar dicho token
     */
    public function buscarToken(Request $request)
    {

        try {
            $token = $request->text;
            $f1 = $request->inicio;
            $f2 = $request->fin;
            if (isset($token) || (isset($f1) && isset($f2))) {
                $tarjetasDao = new TarjetasDAO();
                $buscarTokens = $tarjetasDao->buscarToken($token, $f1, $f2);
                return view('pages.BuscarToken', compact('buscarTokens', 'token', 'f1', 'f2'));
            }
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $_SESSION["idusuario"],
                "Error al eliminar token",
                "Error",
                "Http/Controller/TokenController.php",
                "Tarjeta",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
        }

        return view('pages.BuscarToken', compact('token', 'f1', 'f2'));
    }
    /**
     * Valentina santa osorio
     * Funcion que permite eliminar un token por medio del filtro
     */
    public function eliminarToken(Request $request)
    {

        try {
            LogToken::CrearLog(
                "Ingreso Sistema Tokenizador",
                "En Proceso",
                "Http/Controller/LoginController.php",
                "User",
                "User",
                "",
                ""
            );

            $delete = Tarjeta::findOrFail($request->id_token);
            $delete->delete();
            return redirect()->route('BuscarToken');
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $_SESSION["idusuario"],
                "Error al eliminar token",
                "Error",
                "Http/Controller/TokenController.php",
                "Tarjeta",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
            DB::rollBack();
        }
    }

    public function editarToken(Request $request)
    {
        $estado = $request->estado_id;
        $suscricion = $request->suscripcion_id;
        if ($estado == 'true') {
            $estado = 1;
        } else {
            $estado = 0;
        }

        Tarjeta::where('id', $request->id_token)->update([
            'fechaVencimiento' => $request->fecha_id,
            'estado' => $estado
        ]);
        return redirect()->route('BuscarToken');
    }

    /**
     *
     * Funcion que permite el guardado de un token nuevo
     */
    public function store(Request $request)
    {
        $iduser = Auth::user()->id;
        try {
            DB::beginTransaction();
            $data = $request->validate([
                'numero_tarjeta' => 'required|max:16',
                'nombre_tarjeta' => 'required',
                'fecha_exp' => 'required',
                'suscripcion' => 'required',
                'valor_pago' => 'required',
                'referencia' => '',
                'busqueda' => '',
                'franchise' => '',
                'nombre_cliente' => 'required',
                'apellido1_cliente' => '',
                'apellido2_cliente' => '',
                'cedula_cliente' => '',
                'email_cliente' => 'unique:App\User,email',
                'telefono_cliente' => '',
                'fecha_pagos' => ''
            ]);

            $franchiseId = Franquicia::where('franquicia', $data['franchise'])->select('id')->first();
            $suscripcion = Suscripcion::where('id', $data['suscripcion'])->select('id')->first();
            $cliente = $this->CrearUsuario($data, $iduser);
            if (!is_null($cliente)) {

                $producto = $this->CrearProducto($data, $iduser);

                $existeDek = $this->Getllave();

                if (!is_null($existeDek)) {

                    if ($existeDek->Estado == 1) {

                        $parameterkey =  $existeDek->parameterkey;
                        $tokens = new Tokens();
                        $consumoservicio = ConsumoApiConfig::ConsumoServicios($parameterkey);
                        $cadenaT = substr($data['numero_tarjeta'], -4);
                        $ListaDatosToken = $tokens->CrearToken($data['numero_tarjeta'], $consumoservicio);

                        $token = $this->CrearToken($ListaDatosToken, $data, $iduser);
                        if (!is_null($token)) {
                            $tarjetacreada = $this->CrearTarjeta($data,  $franchiseId,   $cliente, $token, $iduser, 100);
                            if (!is_null($tarjetacreada)) {
                                $ok = $this->CrearPagoMensualComercio($iduser, $data, $suscripcion, $tarjetacreada);
                                if ($ok) {
                                    unset($consumoservicio, $parameterkey);
                                    DB::commit();
                                    return redirect()->route('crearToken')->with("status", "Token: " . $ListaDatosToken[1] . "\n Tarjeta terminada en:  " . $cadenaT);
                                }
                            }
                        }
                    } else {
                        $mensaje = ['Llave_cifrado' => 'La llave de DEK se encuentra desactivada,  por favor activela para poder continuar'];
                        DB::rollBack();
                        return redirect()->route('crearToken')->withErrors($mensaje);
                    }
                } else {
                    $mensaje = ['Llave_cifrado' => 'No existe una llave DEK, cree una para poder realizar el token'];
                    DB::rollBack();
                    return redirect()->route('crearToken')->withErrors($mensaje);
                }
            } else {
                $mensaje = ['withErrors' => 'El cliente no puede ser creado'];
                DB::rollBack();
                return redirect()->route('crearToken')->withErrors($mensaje);
            }
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $_SESSION["idusuario"],
                "Error",
                "Error",
                "Http/Controller/TokenController.php",
                "BB",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
            DB::rollBack();
        }
    }

    private function CrearProducto($data, $iduser)
    {

        try {
            LogToken::CrearLog(
                $iduser,
                "Creacion de Producto",
                "En Proceso",
                "Http/Controller/LoginController.php",
                "Producto",
                ""
            );

            $producto = Producto::create(
                [
                    'valor' => $data['valor_pago'],
                    'codigo' => $data['referencia'],
                ]
            );

            LogToken::CrearLog(
                $iduser,
                "Creacion de Producto",
                "Exito",
                "Http/Controller/TokenController.php",
                "Producto",
                "valor : " . $data['valor_pago'] .
                    " codigo: " . $data['referencia']
            );
            return $producto;
        } catch (\Exception $th) {

            LogToken::CrearLog(
                $iduser,
                "Error al crear Producto",
                "Error",
                "Http/Controller/TokenController.php",
                "Producto",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
        }
    }

    private function CrearPagoMensualComercio($iduser, $data, $suscripcion, $tarjeta)
    {

        try {
            Log::debug("comercio: " . $data['busqueda']);
            LogToken::CrearLog(
                $iduser,
                "Creacion de Pagos Mensales Comercio",
                "En Proceso",
                "Http/Controller/TokenController.php",
                "pagosmensualescomercios",
                "comercio_id : " . $data['busqueda'] . " suscripcion_id: " . $suscripcion->id .
                    " Tarjeta_id: " . $tarjeta->id
            );

            PagosMensualesComercio::create(
                [
                    'comercio_id' => $data['busqueda'],
                    'suscripcion_id' => $suscripcion->id,
                    'Tarjeta_id' => $tarjeta->id,
                    'fechaPagos' => $data['fecha_pagos']
                ]
            );

            LogToken::CrearLog(
                $iduser,
                "Creacion de Pagos Mensales Comercio",
                "Exito",
                "Http/Controller/TokenController.php",
                "pagosmensualescomercios",
                "comercio_id : " . $data['busqueda'] . " suscripcion_id: " . $suscripcion->id .
                    " Tarjeta_id: " . $tarjeta->id
            );

            return true;
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $iduser,
                "Error al crear Pagos Mensales Comercio",
                "Error",
                "Http/Controller/TokenController.php",
                "pagosmensualescomercios",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
        }
    }
    /*
     * Funcion que permite realizar el filtro de comercio para
     * el formulario crear token
     */
    public function foundComercio(Request $request)
    {
        $comercio = $request->text;
        if (isset($comercio)) {
            $listaComercio = Comercio::join('tipoComercio', 'tipoComercio.id', '=', 'comercio.tipoComercio_id')
                ->select('tipoComercio.descripcion', 'comercio.nombre', 'comercio.codVenta', 'comercio.id')
                ->where('comercio.nombre', 'like', '%' . $comercio . '%')
                ->orWhere('comercio.codVenta', 'like', '%' . $comercio . '%')
                ->get();

            return response()->json($listaComercio);
        }
        return response()->json('');
    }
    /**
     * Funcion que permite listar los token y asi mismo realizar un filtro de fechas
     */
    public function listarToken()
    {
        $listaTokens = Tarjeta::join('cliente', 'cliente.id', '=', 'tarjeta.cliente_id')
            ->join('token', 'token.id', '=', 'tarjeta.token_id')
            ->join('franquicia', 'franquicia.id', '=', 'tarjeta.franquicia_id')
            ->select(
                'cliente.nombre',
                'cliente.apellido1',
                'token.token',
                'franquicia.franquicia as franquicia',
                'tarjeta.nombre as tarjeta',
                'tarjeta.fechaVencimiento',
                'tarjeta.estado'
            )
            ->paginate(10);

        return view('pages.listarTokens', compact('listaTokens'));
    }
    /**
     * Valentina santa
     * Funcion que permite listar los token y asi mismo realizar un filtro de fechas
     */
    public function cargarTokens(Request $request)
    {

        $busqueda = $request->busqueda;
        $tokens = Tarjeta::join('comercio', 'comercio.id', '=', 'tarjeta.comercio_id')
            ->join('token', 'token.id', '=', 'tarjeta.token_id')
            ->join('suscripcion', 'suscripcion.id', '=', 'tarjeta.suscripcion_id')
            ->join('franquicia', 'franquicia.id', '=', 'tarjeta.franquicia_id')
            ->select(
                'comercio.nombre',
                'token.token',
                'franquicia.franquicia as franquicia',
                'tarjeta.nombre as tarjeta',
                'tarjeta.fechaVencimiento',
                'suscripcion.TipoSuscripcion'
            )
            ->where('tarjeta.nombre', $busqueda)
            ->first();

        if ($tokens) {
            $msg = false;
            return view('pages.cargarToken', compact('tokens', 'busqueda', 'msg'));
        } else {
            $msg = true;
            return view('pages.cargarToken', compact('tokens', 'busqueda', 'msg'));
        }
    }


    public function GetTarjetaDescrifrada(Request $request)
    {

        $tokenkey =  $request->headers->get('Token-key');
        $ValidaciontokenApi = $this->tokenKeyDao->GetTokenKeyApi($tokenkey);
        $emailcli = $request->email;
        $token = $request->token;
        $tokens = new Tokens();


        try {
            LogToken::CrearLog(
                $emailcli,
                "Inicio de descifrado de tarjeta",
                "En proceso",
                "Http/Controller/TokenController.php",
                "Cliente",
                ""
            );
            if ($ValidaciontokenApi) {
                LogToken::CrearLog(
                    $emailcli,
                    "Validación  del token de seguridad",
                    "Exitoso",
                    "Http/Controller/TokenController.php",
                    "Cliente",
                    ""
                );

                LogToken::CrearLog(
                    $emailcli,
                    "comparacióm entre email y token generado",
                    "En proceso",
                    "Http/Controller/TokenController.php",
                    "Cliente,  Token",
                    ""
                );


                $UsuarioTarjeta = $this->clienteDao->ComparetoEmailCliente($emailcli, $token);
                if (!is_null($UsuarioTarjeta)) {
                    LogToken::CrearLog(
                        $emailcli,
                        "comparacióm entre email y token generado",
                        "Exitoso",
                        "Http/Controller/TokenController.php",
                        "Cliente,  Token",
                        ""
                    );
                    LogToken::CrearLog(
                        $emailcli,
                        "Buscado la llave de cifrado",
                        "En proceso",
                        "Http/Controller/TokenController.php",
                        "Llave",
                        ""
                    );


                    $existeDek = $this->llaveUsuarioDao->BuscarLlaveCifrado();
                    LogToken::CrearLog(
                        $emailcli,
                        "Buscado la llave de cifrado",
                        "Exitoso",
                        "Http/Controller/TokenController.php",
                        "Llave",
                        ""
                    );


                    $parameterkey =  $existeDek->parameterkey;
                    $consumoservicio = ConsumoApiConfig::ConsumoServicios($parameterkey);
                    // $consumoservicio = env('LLAVE', 'pruballavekey');

                    LogToken::CrearLog(
                        $emailcli,
                        "Buscado el pan encrypt con el token",
                        "En proceso",
                        "Http/Controller/TokenController.php",
                        "Llave",
                        ""
                    );

                    $DatosDeserncriptar =  $this->tokenDao->BuscarPanEncrypXToken($token);

                    if (!is_null($DatosDeserncriptar)) {


                        LogToken::CrearLog(
                            $emailcli,
                            "Buscado el pan encrypt con el token",
                            "Exitoso",
                            "Http/Controller/TokenController.php",
                            "Token",
                            ""
                        );

                        $tokenCifrado = $tokens->DescrifrarToken($DatosDeserncriptar->PanEncrypt, $consumoservicio);

                        LogToken::CrearLog(
                            $emailcli,
                            "Token descifrado",
                            "Exitoso",
                            "Http/Controller/TokenController.php",
                            "",
                            ""
                        );


                        $datosRetorno = [
                            "Tarjeta" => $tokenCifrado,
                            "fechaVeTarjeta" =>   $UsuarioTarjeta->fechVencimiento,
                            "Nombre_Cliente" =>   $UsuarioTarjeta->nombre,
                            "Apellido_Cliente" =>   $UsuarioTarjeta->apellido1,
                            "Cedula" =>   $UsuarioTarjeta->cedula,
                            "Telefono" =>   $UsuarioTarjeta->telefono,
                            "Email" =>   $UsuarioTarjeta->email,
                            "Direccion" =>   $UsuarioTarjeta->direccion,

                        ];
                       return response()->json(['code'=>200,'data'=>$datosRetorno],200);

                    } else {
                        LogToken::CrearLog(
                            $emailcli,
                            "Error en el token enviado",
                            "Error",
                            "Http/Controller/TokenController.php",
                            "",
                            ""
                        );

                        return response()->json(['code'=>404,'message'=>'Error el token enviado no coincide!'],404) ;
                    }
                } else {
                    LogToken::CrearLog(
                        $emailcli,
                        "Error en correo y el token enviados",
                        "Error",
                        "Http/Controller/TokenController.php",
                        "",
                        "No se encontraron datos con ese correo y token"
                    );
                    return response()->json(['code'=>404,'message'=>'No se encontraron datos con ese correo y token'],404) ;
                }
            } else {

                LogToken::CrearLog(
                    $emailcli,
                    "Error en el tokenkey de seguridad",
                    "Error",
                    "Http/Controller/TokenController.php",
                    "",
                    "Error el Token-key no corresponde!"
                );
                return response()->json(['code'=>401,'message'=>'Error el Token-key no corresponde!'],401) ;
            }
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $emailcli,
                "Error al Descrifrar el token desde la Api",
                "Error",
                "Http/Controller/TokenController.php",
                "",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
            return response()->json(['code'=>400,'message'=>'Error al Descrifrar el token'],400) ;
        }
    }

    public function show(Request $request)
    {

        $tokenkey =  $request->headers->get('Token-key');
        $email = $request->email;

        try {
            LogToken::CrearLog(
                $email,
                "Validación  del token de seguridad",
                "En proceso",
                "Http/Controller/TokenController.php",
                "Cliente",
                ""
            );

            $ValidaciontokenApi = $this->tokenKeyDao->GetTokenKeyApi($tokenkey);
            if ($ValidaciontokenApi) {
                LogToken::CrearLog(
                    $email,
                    "Validación  del token de seguridad",
                    "Exitoso",
                    "Http/Controller/TokenController.php",
                    "Cliente - token",
                    ""
                );


                LogToken::CrearLog(
                    $email,
                    "Mostrando  las tarjetas del cliente",
                    "En proceso",
                    "Http/Controller/TokenController.php",
                    "Cliente - token",
                    ""
                );
                $tarjetasCliente = $this->tarjetasDao->GetTarjetasCliente($email);

                if (!$tarjetasCliente->isEmpty()) {

                    LogToken::CrearLog(
                        $email,
                        "Mostrando  las tarjetas del cliente",
                        "Exitoso",
                        "Http/Controller/TokenController.php",
                        "Cliente",
                        ""
                    );
                    return response()->json(['code'=>200,'data'=>$tarjetasCliente],200);
                } else {

                    LogToken::CrearLog(
                        $email,
                        "Error al mostrar las tarjetas del cliente",
                        "Exitoso",
                        "Http/Controller/TokenController.php",
                        "Cliente - token",
                        "Correo Incorrect"
                    );
                    return response()->json(['code'=>404,'message'=>'Correo Incorrecto'],404) ;
                }
            } else {
                LogToken::CrearLog(
                    $email,
                    "Error validación  del token de seguridad",
                    "Error",
                    "Http/Controller/TokenController.php",
                    "Cliente - token",
                    "Error el token enviado no coincide!"
                );
                return response()->json(['code'=>401,'message'=>'Error el token enviado no coincide'],401) ;
            }
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $email,
                "Error al mostrar las tarjetas del cliente desde la Api",
                "Error",
                "Http/Controller/TokenController.php",
                "",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
            DB::rollBack();
            return response()->json(['code'=>400,'message'=>'Error al mostrar las tarjetas '],400) ;
        }
    }

    public function ApitopkenizarTarjeta(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $iduser =   $data['email_cliente'];
        $tokenkey =  $request->headers->get('Token-key');

        DB::beginTransaction();


        try {
            LogToken::CrearLog(
                $data['email_cliente'],
                "Validación  del token de seguridad",
                "En proceso",
                "Http/Controller/TokenController.php",
                "Cliente",
                ""
            );

            $ValidaciontokenApi = $this->tokenKeyDao->GetTokenKeyApi($tokenkey);
            if ($ValidaciontokenApi) {
                LogToken::CrearLog(
                    $data['email_cliente'],
                    "Validación  del token de seguridad",
                    "Exitoso",
                    "Http/Controller/TokenController.php",
                    "Cliente",
                    ""
                );

                $cliente = $this->clienteDao->CrearUsuario($data, $iduser);
                if (!is_null($cliente)) {

                    $existeDek = $this->llaveUsuarioDao->Getllave();
                    $parameterkey =  $existeDek->parameterkey;
                    $tokens = new Tokens();

                     $consumoservicio = ConsumoApiConfig::ConsumoServicios($parameterkey);
                    // $this->llaveUsuarioDao->BuscarLlaveNueva();$consumoservicio = env('LLAVE', 'pruballavekey');


                    $valTarjetanueva = $this->tarjetasDao->ComparetoTarjetasTokenizadas($data['numero_tarjeta'], $cliente, $consumoservicio );

                    if ($valTarjetanueva) {



                        $ListaDatosToken = $tokens->CrearToken($data['numero_tarjeta'], $consumoservicio);
                        $token = $this->tokenDao->CrearToken($ListaDatosToken, $data, $iduser);
                        if (!is_null($token)) {


                            $franchiseId = $this->franquiciaDao->BuscarFranquiciaId($data);


                            $tarjetacreada = $this->tarjetasDao->CrearTarjeta($data,  $franchiseId,   $cliente, $token, $iduser, $data['accountid']);
                            if (!is_null($tarjetacreada)) {


                                unset($consumoservicio, $parameterkey);
                                DB::commit();
                                $datosRetorno = [
                                    "Tarjeta" => $data['numero_tarjeta'],
                                    "Token" => $ListaDatosToken[1]
                                ];
                            return response()->json(['code'=>200,'data'=>$datosRetorno],200);

                            }
                        }
                    } else {
                        LogToken::CrearLog(
                            $data['email_cliente'],
                            "Tarjeta ya tokenizada",
                            "exito",
                            "Http/Controller/TokenController.php",
                            "tarjetas",
                            ""
                        );
                        return response()->json(['code'=>404,'message'=>'Tarjeta ya tokenizada!'],404) ;

                    }
                }
            } else {
                LogToken::CrearLog(
                    $data['email_cliente'],
                    "Error validación  del token de seguridad",
                    "Error",
                    "Http/Controller/TokenController.php",
                    "Cliente",
                    "Error el token enviado no coincide!"
                );
                return response()->json(['code'=>401,'message'=>'Error el Token-key no corresponde!'],401) ;
            }
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $data['email_cliente'],
                "Error al Tokenizar la tarjeta desde la Api",
                "Error",
                "Http/Controller/TokenController.php",
                "",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
            DB::rollBack();
            return response()->json(['code'=>400,'message'=>'Error al Tokenizar la tarjeta'],400) ;
        }
    }

    public function destroy(Request $request)
    {
        DB::beginTransaction();
        $tokenkey =  $request->headers->get('Token-key');
        $ValidaciontokenApi = $this->tokenKeyDao->GetTokenKeyApi($tokenkey);
        $data = json_decode($request->getContent(), true);


        try {
            LogToken::CrearLog(
                $data['email'],
                "Validación  del token de seguridad",
                "En proceso",
                "Http/Controller/TokenController.php",
                "Cliente",
                ""
            );

            if ($ValidaciontokenApi) {

                LogToken::CrearLog(
                    $data['email'],
                    "Validación  del token de seguridad",
                    "Exitoso",
                    "Http/Controller/TokenController.php",
                    "Cliente",
                    ""
                );
                $data = json_decode($request->getContent(), true);
                if ($this->clienteDao->ConfirmarCorreoCliente($data['email'])) {
                    LogToken::CrearLog(
                        $data['email'],
                        "Confirmación del correo del cliente",
                        "Exitoso",
                        "Http/Controller/TokenController.php",
                        "Token",
                        ""
                    );
                    LogToken::CrearLog(
                        $data['email'],
                        "Eliminacion de la  tarjeta",
                        "En proceso",
                        "Http/Controller/TokenController.php",
                        "Token",
                        ""
                    );

                    $token = $this->tarjetasDao->EliminarTarjeta($data['token']);

                    if (!is_null($token)) {
                        LogToken::CrearLog(
                            $data['email'],
                            "Eliminación de la  tarjeta",
                            "Exitoso",
                            "Http/Controller/TokenController.php",
                            "Token",
                            ""
                        );
                        LogToken::CrearLog(
                            $data['email'],
                            "Eliminación del token",
                            "En proceso",
                            "Http/Controller/TokenController.php",
                            "Token",
                            ""
                        );
                        if ($this->tokenDao->EliminartokenCliente($token->id)) {
                            LogToken::CrearLog(
                                $data['email'],
                                "Eliminación del token",
                                "Exitoso",
                                "Http/Controller/TokenController.php",
                                "Token",
                                ""
                            );
                            DB::commit();
                            return response()->json(['code'=>200,'data'=>'Datos eliminados'],200);
                        } else {
                            LogToken::CrearLog(
                                $data['email'],
                                "Error en la Eliminación del token",
                                "Error",
                                "Http/Controller/TokenController.php",
                                "Cliente",
                                "Se a presentado un error en la elimación del token"
                            );
                            return response()->json(['code'=>400,'message'=>'Se a presentado un error en la elimación del token'],400) ;

                        }
                    } else {
                        LogToken::CrearLog(
                            $data['email'],
                            "Error en la Eliminación de la tarjeta",
                            "Error",
                            "Http/Controller/TokenController.php",
                            "Cliente",
                            "Error el token enviado es incorrecto"
                        );

                        DB::rollBack();
                        return response()->json(['code'=>404,'message'=>'Error el token enviado es incorrecto'],404) ;

                    }
                } else {
                    LogToken::CrearLog(
                        $data['email'],
                        "Error en la confirmación del correo enviado",
                        "Error",
                        "Http/Controller/TokenController.php",
                        "Cliente",
                        "Error el correo no corresponde!"
                    );
                    DB::rollBack();

                    return response()->json(['code'=>404,'message'=>'Error el correo no corresponde!'],404) ;

                }
            } else {
                LogToken::CrearLog(
                    $data['email'],
                    "Error validación  del token de seguridad",
                    "Error",
                    "Http/Controller/TokenController.php",
                    "Cliente",
                    "Error el token enviado no coincide!"
                );
                DB::rollBack();
                return response()->json(['code'=>401,'message'=>'Error el Token-key no corresponde!'],401) ;
            }
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $data['email'],
                "Error al eliminar la tarjeta desde la Api",
                "Error",
                "Http/Controller/TokenController.php",
                "",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
            DB::rollBack();
            return response()->json(['code'=>400,'message'=>'Error al eliminar los datos'],400) ;
        }
    }
}
