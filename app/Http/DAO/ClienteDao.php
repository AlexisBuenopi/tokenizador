<?php
namespace App\Http\DAO;
use Illuminate\Support\Facades\DB;
use App\Http\Clases\LogToken;
use App\Cliente;
class  ClienteDao{


    public  function CrearUsuario($data, $iduser){
        LogToken::CrearLog(
            $iduser,
            "Creacion de Cliente",
            "En Proceso",
            "Http/DAO/ClienteDao.php",
            "Cliente",
            "nombre : " . $data['nombre_cliente'] .
                " apellido1: " . $data['nombre_cliente'] . " apellido2: " . $data['apellido2_cliente'] .
                " cedula: " . $data['cedula_cliente'] . " email: " . $data['email_cliente'] .
                " telefono: " . $data['telefono_cliente']
        );

        try {

            $cliente =  DB::table('cliente')->where('email', "=", $data['email_cliente'])
                ->select('nombre', 'apellido1', 'cedula', 'email', 'id')
                ->first();

            if (is_null($cliente)) {
                $cliente   = Cliente::create(
                    [
                        'nombre' => $data['nombre_cliente'],
                        'apellido1' => $data['apellido1_cliente'],
                        'apellido2' => $data['apellido2_cliente'],
                        'cedula' => $data['cedula_cliente'],
                        'email' => $data['email_cliente'],
                        'telefono' => $data['telefono_cliente'],
                        'ipcliente' => "",
                        'direccion' => $data['dircliente'],

                    ]
                );

                LogToken::CrearLog(
                    $iduser,
                    "Creacion de Cliente",
                    "Exito",
                    "Http/DAO/ClienteDao.php",
                    "Cliente",
                    "nombre : " . $data['nombre_cliente'] .
                        " apellido1: " . $data['nombre_cliente'] . " apellido2: " . $data['apellido2_cliente'] .
                        " cedula: " . $data['cedula_cliente'] . " email: " . $data['email_cliente'] .
                        " telefono: " . $data['telefono_cliente']
                );
            } else {
                LogToken::CrearLog(
                    $iduser,
                    "Creacion de Cliente",
                    "Cliente ya creado",
                    "Http/DAO/ClienteDao.php",
                    "Cliente",
                    "nombre : " . $data['nombre_cliente'] .
                        " apellido1: " . $data['nombre_cliente'] . " apellido2: " . $data['apellido2_cliente'] .
                        " cedula: " . $data['cedula_cliente'] . " email: " . $data['email_cliente'] .
                        " telefono: " . $data['telefono_cliente']
                );
            }


            return $cliente;
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $iduser,
                "Error al crear Cliente",
                "Error",
                "Http/DAO/ClienteDao.php",
                "Cliente",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
        }
    }

    public function ConfirmarCorreoCliente($email){
        $cliente =  DB::table('cliente')->where('email', $email)
            ->select('nombre', 'apellido1', 'cedula', 'email', 'id')
            ->first();
        if (!is_null($cliente)) {
            return true;
        } else {
            return false;
        }
    }

    public function ComparetoEmailCliente($email, $token)
    {
        $cliente =  DB::table('cliente')
            ->join('tarjeta', 'cliente.id', '=', 'tarjeta.cliente_id')
            ->join('token', 'token.id', '=', 'tarjeta.token_id')
            ->where('cliente.email', '=', $email)
            ->where('token.Token', '=', $token)
            ->select(
                'cliente.nombre',
                'cliente.apellido1',
                'cliente.cedula',
                'cliente.email',
                'cliente.id',
                'cliente.telefono',
                'cliente.direccion',
                'tarjeta.fechaVencimiento as fechVencimiento'
            )
            ->first();

        return $cliente;
    }

}
