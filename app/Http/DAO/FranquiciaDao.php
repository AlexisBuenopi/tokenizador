<?php
namespace App\Http\DAO;
use Illuminate\Support\Facades\DB;
use App\Franquicia;
class  FranquiciaDao{

    public  function BuscarFranquiciaId($data)
    {
        $franchiseId = Franquicia::where('franquicia', $data['franchise'])->select('id')->first();
        return $franchiseId;
    }
}
