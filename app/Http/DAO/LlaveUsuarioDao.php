<?php
namespace App\Http\DAO;

use App\LlaveUsuario;
use Illuminate\Support\Facades\DB;

class  LlaveUsuarioDao{

    public function Getllave()
    {
        $existeDek = DB::table('llaveusuario')->where('Esdek', 1)
            ->select('parameterkey', 'id', 'Esdek', 'Estado')
            ->first();

        return $existeDek;
    }


    public function BuscarLlaveCifrado()
    {
        $existeDek = DB::table('llaveusuario')->where('Esdek', 1)
            ->select('parameterkey', 'id', 'Esdek', 'Estado')
            ->first();
        return $existeDek;
    }


    public function CrearLlaveCifrado($newLlave, $Esdek,$EsNuevadek ){
        DB::table('llaveusuario')->insert([

            'idusuario' => 1,
            'parameterkey' => $newLlave,
            'Estado' => 1,
            'Esdek' => $Esdek,
            'EsNuevadek' => $EsNuevadek,
            'created_at' => now(),
            'updated_at' => now()
        ]);

    }
    public function EliminarLlaveCifrado($llaveOld){
        DB::table('llaveusuario')->where('parameterkey', '=', $llaveOld)->delete();
    }

    public function ActualizarLlaveCifrado($idLlave, $Esdek, $EsNuevadek){

        LlaveUsuario::where('parameterkey', $idLlave)
        ->update([
            'Esdek' => $Esdek,
            'EsNuevadek' => $EsNuevadek
        ]);
    }

    public function BuscarLlaveNueva()
    {
        $existeDek = DB::table('llaveusuario')->where('EsNuevadek', 1)
            ->select('parameterkey', 'id', 'Esdek', 'Estado')
            ->first();
        return $existeDek ->parameterkey;
    }

}
