<?php

namespace App\Http\DAO;
use App\Tarjeta;
use Illuminate\Support\Facades\DB;
use App\Http\Clases\LogToken;
use App\Http\Clases\Tokens;
use Carbon\Carbon;
class TarjetasDAO {

    public function buscarToken($token, $f1, $f2) {
        $buscarTokens = Tarjeta::join('token','token.id','=','tarjeta.token_id')
        ->join('franquicia', 'franquicia.id', '=', 'tarjeta.franquicia_id')
        ->join('cliente', 'cliente.id', '=', 'tarjeta.cliente_id')
        ->select('cliente.telefono','cliente.nombre','cliente.email','tarjeta.id','token.token', 'franquicia.franquicia as franquicia',
        'tarjeta.nombre as tarjeta','tarjeta.fechaVencimiento', 'tarjeta.estado', 'tarjeta.created_at');

        if($token != '') {
            $buscarTokens = $buscarTokens->where('tarjeta.nombre', 'like', '%'. $token .'%')
            ->orWhere('token.token', 'like', '%'. $token .'%');
        }

        if ($f1 != '' && $f2 == '') {
            $date = date("Y-m-d");
            $buscarTokens = $buscarTokens->whereBetween('tarjeta.created_at', [$f1, $date]);
        }

        if ($f1 == '' && $f2 != '') {
            $buscarTokens = $buscarTokens->whereBetween('tarjeta.created_at', [$date, $f2]);
        }

        if ($f1 != '' && $f2 != '') {
            $buscarTokens = $buscarTokens->whereBetween('tarjeta.created_at', [$f1, $f2]);
        }

        return $buscarTokens->paginate(2);
    }

    public function GetTarjetasCliente($email){
        $ListaTarjetas = DB::table('tarjeta')
            ->join('cliente', 'cliente.id', '=', 'tarjeta.cliente_id')
            ->join('token', 'token.id', '=', 'tarjeta.token_id')
            ->join('franquicia', 'franquicia.id', '=', 'tarjeta.franquicia_id')
            ->select('tarjeta.id', 'tarjeta.nombre', 'token.Token', 'tarjeta.AccountId', "tarjeta.numTarjeta", "franquicia.franquicia")
            ->where('cliente.email', '=', $email)
            ->get();

        return  $ListaTarjetas;
    }

    public function ComparetoTarjetasTokenizadas($tarjeta, $cliente, $keyCifrado){

        LogToken::CrearLog(
            $cliente->email,
            "comparando tarjetas",
            "En proceso",
            "Http/DAO/TarjetasDAO.php",
            "Cliente",
            ""
        );
        $tokens = new Tokens();

        $val_compare = true;
        $ListaTarjetas = DB::table('tarjeta')
            ->join('token', 'token.id', '=', 'tarjeta.token_id')
            ->where('cliente_id', $cliente->id)
            ->select( 'PanEncrypt')->get();

        if (!is_null($ListaTarjetas)) {

            foreach ($ListaTarjetas as $key => $value) {

                $tokenCifrado = $tokens->DescrifrarToken($value->PanEncrypt, $keyCifrado);
                if (trim($tarjeta) == trim($tokenCifrado)) {
                    $val_compare = false;
                }
            }
        } else {
            $val_compare = true;
        }
        LogToken::CrearLog(
            $cliente->email,
            "comparando tarjetas",
            "exitoso",
            "Http/DAO/TarjetasDAO.php",
            "Cliente",
            ""
        );


        return $val_compare;
    }

    public function CrearTarjeta($data,  $franchiseId,  $cliente, $token, $iduser, $accountid) {
        try {
            LogToken::CrearLog(
                $iduser,
                "Creacion de Tarjeta",
                "En Proceso",
                "Http/DAO/TarjetasDAO.php",
                "Tarjeta",
                "estado : 1" . " AccountId: " . $accountid . " nombre: " . $data['nombre_tarjeta'] .
                    " Tarjeta: " . substr($data['numero_tarjeta'], -4) . " fechaVencimiento: " . $data['fecha_exp']
                    . " franquicia_id: " . $franchiseId->id . " cliente_id: " . $cliente->id . " token_id: " . $token->id
            );

            $tarjetan =  Tarjeta::create(
                [
                    'numTarjeta' => $data['numero_tarjeta'],
                    'estado' => 1,
                    'AccountId'  => $accountid,
                    'nombre' => $data['nombre_tarjeta'],
                    'fechaVencimiento' => $data['fecha_exp'],
                    'franquicia_id' => $franchiseId->id,
                    'cliente_id' => $cliente->id,
                    'token_id' => $token->id
                ]
            );

            LogToken::CrearLog(
                $iduser,
                "Creacion de Tarjeta",
                "Exito",
                "Http/DAO/TarjetasDAO.php",
                "Tarjeta",
                "estado : 1" . " AccountId: " . $accountid . " nombre: " . $data['nombre_tarjeta'] .
                    " Tarjeta: " . substr($data['numero_tarjeta'], -4) . " fechaVencimiento: " . $data['fecha_exp']
                    . " franquicia_id: " . $franchiseId->id . " cliente_id: " . $cliente->id . " token_id: " . $token->id
            );

            return $tarjetan;
        } catch (\Exception $th) {
            LogToken::CrearLog(
                $iduser,
                "Error al crear Tarjeta",
                "Error",
                "Http/DAO/TarjetasDAO.php",
                "Tarjeta",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
        }
    }

    public function EliminarTarjeta($token){
        try {

            $idtoken = DB::table('token')->where('token', "=", $token)
                ->select('id')
                ->first();

            if (!is_null($idtoken)) {
                $idtarjeta = DB::table('tarjeta')->where('token_id', "=", $idtoken->id)
                    ->select('id')
                    ->first();

                if (!is_null($idtarjeta)) {
                    DB::table('tarjeta')->where('id', '=', $idtarjeta->id)->delete();
                }
            }

            return  $idtoken;
        } catch (\Exception $th) {
            LogToken::CrearLog(
                "",
                "Error al eliminar tarjeta",
                "Error",
                "Http/DAO/TarjetasDAO.php",
                "Cliente",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
            return null;
        }
    }

    public function GetTarjetas(){
        $ListaTarjetas = DB::table('tarjeta')
        ->join('token', 'token.id', '=', 'tarjeta.token_id')
        ->select('token.PanEncrypt', 'token.Token', 'token.id as token', 'tarjeta.id as tarjeta')
        ->get();

        return $ListaTarjetas;
    }

    public function UpdateFechaTarjeta($idTarjeta){

        Tarjeta::where('id', $idTarjeta)
        ->update(['updated_at' => now()]);
    }


    public function GetTarjetasNoActulizadas(){
        $fechaactual = Carbon::now()->subDays(30)->format('Y-m-d');
        $ListaTarjetas = DB::table('tarjeta')
        ->join('token', 'token.id', '=', 'tarjeta.token_id')
        ->select('token.PanEncrypt', 'token.Token', 'token.id as token', 'tarjeta.id as tarjeta')
        ->whereDate('tarjeta.updated_at', '<',$fechaactual)
        ->get();

        return $ListaTarjetas;
    }

}
