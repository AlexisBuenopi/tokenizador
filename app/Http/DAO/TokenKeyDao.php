<?php
namespace App\Http\DAO;
use Illuminate\Support\Facades\DB;
class TokenKeyDao{

    public function GetTokenKeyApi($token)
    {

        $tokenkey = DB::table('token_key')->where('tokenkey', $token)
            ->select('id', 'tokenkey')
            ->first();
        if (!is_null($tokenkey)) {
            return  true;
        } else {
            return  false;
        }
    }

}
