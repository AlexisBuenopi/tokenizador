<?php
namespace App\Http\DAO;
use Illuminate\Support\Facades\DB;
use App\Token;
use App\Http\Clases\LogToken;
class TokensDao{

    public function BuscarPanEncrypXToken($token){

        $DatosDeserncriptar = DB::table('token')->where('Token', $token)
            ->select('PanEncrypt', 'id')
            ->first();
        return  $DatosDeserncriptar;
    }

    public function CrearToken($tokenCifrado, $data, $iduser){
        LogToken::CrearLog(
            $iduser,
            "Creacion de token",
            "En Proceso",
            "Http/DAO/TokensDao.php",
            "Token",
            ""
        );


        try {
            $token  = Token::create(
                [
                    'PanEncrypt' =>  $tokenCifrado[0],
                    'token' => $tokenCifrado[1],
                ]
            );

            LogToken::CrearLog(
                $iduser,
                "Creacion de Token",
                "Exito",
                "Http/DAO/TokensDao.php",
                "Token",
                "PanEncrypt : " .
                    " token: "
            );

            return $token;
        } catch (\Exception  $th) {
            LogToken::CrearLog(
                $iduser,
                "Error al crear Token",
                "Error",
                "Http/DAO/TokensDao.php",
                "Token",
                "Error " . $th->getMessage() .
                    " \n La excepción se creó en la línea: " . $th->getLine() .
                    " \n El código de excepción es: " . $th->getCode() .
                    " \n En el archivo " . $th->getFile()
            );
        }
    }

    public function EliminartokenCliente($idtoken) {
        try {
            DB::table('token')->where('id', '=', $idtoken)->delete();
            return true;
        } catch (\Exception $th) {
            return false;
        }
    }

    public function ActulizarPanEncryptToken($newPanEncrypt, $idToken){

           Token::where('id', $idToken)
                ->update(['PanEncrypt' => $newPanEncrypt]);

    }
}
