<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TokenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numero_tarjeta' => 'required|max:16',
            'nombre_tarjeta' => 'required',
            'fecha_exp' => 'required',
            'fecha_pagos' => '',
            'suscripcion' => 'required',
            'valor_pago' => 'required',
            'referencia' => '',
            'busqueda' => '',
            'franchise' => 'required',
            'nombre_cliente'=>'required',
            'apellido1_cliente'=>'',
            'apellido2_cliente'=>'',
            'cedula_cliente'=>'',
            'email_cliente'=>'',
            'telefono_cliente'=>'',
        ];
    }
}
