<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LlaveUsuario extends Model
{
    protected $fillable = [
        'id', 'parameterkey','idusuario','Estado','Esdek','EsNuevaLlave'
       ];
    protected $table = 'llaveusuario';

}
