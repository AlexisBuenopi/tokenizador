<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PagosMensualesComercio extends Model
{
    protected $fillable = [
        'id', 'tarjeta_id','Comercio_id','suscripcion_id','fechaPagos'
       ];
      protected $table = 'pagosmensualescomercios';
}
