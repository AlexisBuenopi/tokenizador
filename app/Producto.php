<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    /**
     * The attributes that are mass assignable.s
     *
     * @var array 
     */
    protected $fillable = [
        'id', 'nombre','descripcion','valor','codigo','tipoComercio_id'
       ];
      protected $table = 'producto';
      
        //A uno a finger
        public function Comercio()
        {
            return $this->hasOne(Comercio::class);
        }
}
