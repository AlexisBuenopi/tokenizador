<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suscripcion extends Model
{
    /**
     * The attributes that are mass assignable.s
     *
     * @var array
     */
    protected $fillable = [
        'id', 'tipoSuscripcion'
       ];
    protected $table = 'suscripcion';

    //A uno a finger
    public function Tarjeta()
    {
        return $this->hasMany(Tarjeta::class);
    }
}
