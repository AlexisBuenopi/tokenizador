<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Http\Clases\ConsumoApiConfig;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
class Tarjeta extends Model
{   
    use SoftDeletes; //Implementamos 

    protected $dates = ['deleted_at']; 
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $dateFormat = 'Y-m-d';
    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'id','nombre','numTarjeta','fechaVencimiento','tarjetaCol','token_id',
        'franquicia_id','AccountId','estado','cliente_id','created_at'
       ];
  
    protected $table = 'tarjeta';

    //A uno a tarjeta
    public function Estado()
    {
        return $this->hasOne(Estado::class);
    }
    //A uno a Tarjeta
    public function Token()
    {
        return $this->hasOne(Token::class);
    }
    //A uno a Tarjeta
    public function Cifrado()
    {
        return $this->hasOne(Cifrado::class);
    }
     //A uno a Tarjeta
     public function Franquicia()
     {
         return $this->hasOne(Franquicia::class);
     }

    /**
    * Mutator que permite el 
    * @param  string  $value
    * @return void
    */
    
    
    public function setNumTarjetaAttribute($value)
    {
       // $consumoservicio = ConsumoApiConfig::ConsumoEnvioParamatros($request->key,$request->key_value,$request->KMS_key, $dek); 
      //  $this->attributes['numTarjeta'] = openssl_encrypt($value,getenv('DES_METHOD'), getenv('SHA256_KEY'), 0, getenv('DES_IV'));
      $this->attributes['numTarjeta'] = "************" .substr($value, -4);
    }
    /**
    * Mutator que permite el 
    * @param  string  $value
    * @return void
    */
    public function getNumtarjetaAttribute($value)
    { 
        return  openssl_decrypt($value,getenv('DES_METHOD'), getenv('SHA256_KEY'), 0, getenv('DES_IV'));
    }
    /**
    * Mutator que permite el 
    * @param  string  $value
    * @return void
    */
    //public function setCvvAttribute($value)
    //{
       // $key = pack("H*", getenv('DES3_KEY'));
       // $this->attributes['cvv'] = openssl_encrypt($value, getenv('DES_METHOD'), $key, 0,getenv('DES_IV'));
    //}
    /**
    * Mutator que permite el 
    * @param  string  $value
    * @return void
    */
    //public function getCvvAttribute($value)
    //{
       // $key = pack("H*", getenv('DES3_KEY'));
        //return openssl_decrypt($value, getenv('DES_METHOD'), $key, 0,getenv('DES_IV'));        
    //}

    public function toArray()
    {
        $toArray = parent::toArray();
        $toArray['numTarjeta'] = $this->numTarjeta;
        return $toArray;
    }

}