<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoComercio extends Model
{
    /**
     * The attributes that are mass assignable.s
     *
     * @var array
     */
    protected $fillable = [
        'id', 'descripcion'
       ];
    protected $table = 'tipoComercio';

    //A uno a tarjeta
    public function Comercio()
    {
        return $this->hasMany(Comercio::class);
    } 
}
