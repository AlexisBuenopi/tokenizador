<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
     /**
     * The attributes that are mass assignable.s
     *
     * @var array
     */
    protected $fillable = [
        'id', 'PanEncrypt','token',
       ];

    protected $table = 'token';
      
        //A uno a finger
        public function Finger()
        {
            return $this->hasOne(Finger::class);
        }
}
