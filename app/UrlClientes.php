<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UrlClientes extends Model
{
    protected $fillable = [
        'id', 'url','estado','fechacreacion'
       ];
      protected $table = 'UrlClientes';
      
}
