<?php

namespace App;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;

class historialcontrasena extends Model
{
    protected $fillable = [
        'idusuario', 'password','Estado','fechaCreacion'
       ];

       protected $table = 'historialcontrasena';
      
       //A uno a finger
       public function historialcontrasena()
       {
           return $this->hasOne(User::class);
       }

}
