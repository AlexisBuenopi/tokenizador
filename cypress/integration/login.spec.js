import faker from 'faker'
const email = faker.internet.email()
const password = faker.internet.password()

describe('login y register', () => {


    it("Log in programatically", () => {
        //ingreso incorrecto
        cy.visit("http://127.0.0.1:8000/login");
        cy.get('input[name=email]').should('exist')
        cy.get('input[name=password]').should('exist')
        cy.get('input[name=email]').type(email)
        cy.get('input[name=password]').type(password)
        cy.get('button').contains('Iniciar sesion').click()
        cy.get('body').should('contain', ' Estas credenciales no coinciden con nuestros registros.')
        cy.wait(4000);

        cy.visit("http://127.0.0.1:8000/login");
        cy.get('input[name=code]').should('not.exist');
        cy.get('input[name=email]').should('exist')
        cy.get('input[name=password]').should('exist')
        cy.get('input[name=email]').type('valen970211@gmail.com')
        cy.get('input[name=password]').type('secret')
        cy.wait(4000);
        cy.get('button').contains('Iniciar sesion').click()
        cy.wait(4000); 
        
        //ingreso de codigo de verificacion
        cy.get('input[name=code]').should('exist');
        cy.get('input[name=code]').type('codigo');
        cy.get('button').contains('Iniciar sesion').click()
        cy.wait(4000);
       // cy.get('button').contains('Salir').click()

    });

});



// describe("Form", () => {
//     before(() => {
//         cy.exec(
//             "if [ -f .env.backed_up_by_cypress ]; then echo 'It seems Cypress did not finish last time. Check you .env file to prevent loss of data.'; exit 1; fi"
//         );
//         cy.exec("cp .env .env.backed_up_by_cypress");
//         cy.exec("cp .env.cypress .env");
//         cy.exec("php artisan migrate:fresh");
//         cy.exec("php artisan db:seed --class CypressSeeder");
//     });

//     it("Guest can not view the form", () => {
//         cy.visit("/form");
//         cy.url().should("include", "/login");
//     });

//     it("The seeded user can login", () => {
//         cy.visit("/login");
//         cy.get("#email").type("some_user@example.com");
//         cy.get("#password").type("somePassword");
//         cy.get("button[type=submit]").click();
//         cy.url().should("include", "/home");
//     });

//     it("Log in programatically", () => {
//         cy.loginWithCredentials({
//             email: "some_user@example.com",
//             password: "somePassword"
//         });

//         cy.visit("/home")
//             .url()
//             .should("include", "/home");
//     });

//     after(() => {
//         cy.exec("mv .env.backed_up_by_cypress .env");
//     });
// });