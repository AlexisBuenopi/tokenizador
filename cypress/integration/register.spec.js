import faker from 'faker'
const email = faker.internet.email()
const password = faker.internet.password()

//registro incorrecto - Password invalido
  it('gives validation error', () => {
    cy.visit("http://127.0.0.1:8000/login");
    cy.get('input[name=code]').should('not.exist');
    cy.get('input[name=email]').should('exist')
    cy.get('input[name=password]').should('exist')
    cy.get('input[name=email]').type('valen970211@gmail.com')
    cy.get('input[name=password]').type('secret')

    cy.wait(2000);

    cy.get('button').contains('Iniciar sesion').click()

    cy.wait(2000);
    //ingreso de codigo de verificacion
    cy.get('input[name=code]').should('exist');
    cy.get('input[name=code]').type('codigo');
    cy.get('button').contains('Iniciar sesion').click()
 
    //registro 
    cy.visit('http://127.0.0.1:8000/userregister')
    cy.get('input[name=name]').type(faker.name.findName()) 
    cy.get('input[name=cargo]').type('admin')
    cy.get('input[name=email]').type(email)
    cy.get('input[name=password]').type('123')
    cy.get('input[name=password_confirmation]').type('123')
    cy.get('button').contains('Crear cuenta').click()
    cy.get('body').should('contain', 'La contraseña debe contener más de 8 caracteres')
    cy.wait(4000);
    cy.visit('http://127.0.0.1:8000/userregister')
    cy.get('input[name=name]').type(faker.name.findName())
    cy.get('input[name=cargo]').type('admin')
    cy.get('input[name=email]').type(email)
    cy.get('input[name=password]').type('222222222')
    cy.get('input[name=password_confirmation]').type('111111111')
    cy.get('button').contains('Crear cuenta').click()
    cy.get('body').should('contain', 'La confirmación de contraseña no coincide')
    cy.wait(4000);
    cy.visit('http://127.0.0.1:8000/userregister')
    cy.get('input[name=name]').type(faker.name.findName())
    cy.get('input[name=cargo]').type('admin1')
    cy.get('input[name=email]').type(email)
    cy.get('input[name=password]').type(password)
    cy.get('input[name=password_confirmation]').type(password)
    cy.get('button').contains('Crear cuenta').click()
    // cy.contains('Logout').click()
});

