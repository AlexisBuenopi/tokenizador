import faker from 'faker'
const email = faker.internet.email()
const password = faker.internet.password()

describe('register', () => {
    
    // token creado correctamente
    it('successfully create token', () => {
        cy.visit('http://127.0.0.1:8000/store')
        cy.get('input[name=numero_tarjeta').type('123')
        cy.get('input[name=fecha_exp').type('marzo de 2020')
        cy.get('input[name=cvv').type('123')
        cy.get('input[name=valor_pago').type('55000')
        cy.get('input[name=referencia').type('123')
        cy.get('input[name=comercio').type('valentina santa - 41 - Gateway')
        cy.get('input[name=fecha_pagos').type('02/05/2020')
        cy.get('input[name=nombre_cliente').type('pepito')
        cy.get('input[name=apellido1_cliente').type('perez')
        cy.get('input[name=apellido2_cliente').type('santa')
        cy.get('input[name=email_cliente').type(email)
        cy.get('input[name=cedula_cliente').type('1094961018')
        cy.get('input[name=telefono_cliente').type('3172987852')
        cy.get('button').contains('CREAR TOKEN').click()
    });

});