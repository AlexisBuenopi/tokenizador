<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finger', function (Blueprint $table) {
            $table->id('id');
            $table->string('user_agent');
            $table->string('language');
            $table->string('color_depth');
            $table->string('device_memory');
            $table->string('hardware_concurrency');
            $table->string('resolution');
            $table->string('available_resolution');
            $table->string('timezone_offset');
            $table->string('session_storage');
            $table->string('timezone');
            $table->string('local_storage');
            $table->string('indexed_db');
            $table->string('open_database');
            $table->string('cpu_class');
            $table->string('navigator_platform');
            $table->string('regular_plugins');
            $table->string('canvas');
            $table->string('webg1');
            $table->string('web1_vendor');
            $table->string('adblock');
            $table->string('has_lied_languages');
            $table->string('has_lied_resolution');
            $table->string('has_lied_os');
            $table->string('has_lied_browser');
            $table->string('touch_support');
            $table->string('js_fonts');
            $table->string('audio_bfb');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finger');
    }
}
