<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeingKeyComercio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comercio', function(Blueprint $table)
        {
            $table->bigInteger('producto_id')->unsigned()->nullable();
            $table->foreign('producto_id')->references('id')->on('producto');
            $table->bigInteger('tipoComercio_id')->unsigned()->nullable();
            $table->foreign('tipoComercio_id')->references('id')->on('tipoComercio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comercio', function (Blueprint $table) {
            $table->dropForeign('comercio_producto_id_foreign');
            $table->dropForeign('comercio_tipoComercio_id_foreign');

        });
    }
}
