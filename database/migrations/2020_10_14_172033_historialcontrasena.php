<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Historialcontrasena extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
  
    Schema::create('historialcontrasena', function (Blueprint $table) {
            $table->id();          
            $table->string('password');
            $table->boolean('Estado');       
            $table->date('fechaCreacion');
            $table->timestamps();
        });
        
 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historialcontrasena');
    }
}
