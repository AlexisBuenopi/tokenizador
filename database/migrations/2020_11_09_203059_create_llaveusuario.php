<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLlaveusuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('llaveusuario', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idusuario');
            $table->foreign('idusuario', 'fk_idusuario_llaveusuario')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('parameterkey');
            $table->boolean('Estado');
            $table->boolean('Esdek');
            $table->boolean('EsNuevadek');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('llaveusuario');
    }
}
