<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeingKeyUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->bigInteger('codigo_seguridad_id')->unsigned()->nullable();
            $table->foreign('codigo_seguridad_id')->references('id')->on('codigo_seguridad');
            $table->unsignedBigInteger('rol_id');
            $table->foreign('rol_id', 'fk_orol_usuario')->references('id')->on('rol')->onDelete('cascade')->onUpdate('cascade');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_codigo_seguridad_id_foreign');
        });
    }
}
