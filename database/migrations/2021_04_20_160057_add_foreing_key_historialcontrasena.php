<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeingKeyHistorialcontrasena extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historialcontrasena', function(Blueprint $table)
        {        

            $table->bigInteger('idusuario')->unsigned()->nullable();
            $table->foreign('idusuario')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historialcontrasena', function (Blueprint $table) {
            $table->dropForeign('historialcontrasena_idusuario_foreign');       
        });
    }
}
