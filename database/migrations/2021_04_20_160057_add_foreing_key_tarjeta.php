<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeingKeyTarjeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tarjeta', function(Blueprint $table)
        {
            $table->bigInteger('token_id')->unsigned()->nullable();
            $table->foreign('token_id')->references('id')->on('token');

            $table->bigInteger('cliente_id')->unsigned()->nullable();
            $table->foreign('cliente_id')->references('id')->on('cliente');

            $table->bigInteger('franquicia_id')->unsigned()->nullable();
            $table->foreign('franquicia_id')->references('id')->on('franquicia');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tarjeta', function (Blueprint $table) {
            $table->dropForeign('tarjeta_token_id_foreign');
            $table->dropForeign('tarjeta_cliente_id_foreign');
            $table->dropForeign('tarjeta_cifrado_id_foreign');
            $table->dropForeign('tarjeta_franquicia_id_foreign');
  
        });
    }
}
