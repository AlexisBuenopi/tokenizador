<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeingKeyPagosMensualesComercio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pagosmensualescomercios', function(Blueprint $table)
        {
            $table->bigInteger('comercio_id')->unsigned()->nullable();
            $table->foreign('comercio_id')->references('id')->on('comercio');

            $table->bigInteger('suscripcion_id')->unsigned()->nullable();
            $table->foreign('suscripcion_id')->references('id')->on('suscripcion');
       
            $table->bigInteger('Tarjeta_id')->unsigned()->nullable();
            $table->foreign('Tarjeta_id')->references('id')->on('tarjeta');
       



       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('PagosMensualesComercio', function (Blueprint $table) {
         
        });
    }
}
