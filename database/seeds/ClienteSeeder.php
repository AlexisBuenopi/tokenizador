<?php

use Illuminate\Database\Seeder;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cliente')->insert([
            'id' => 1,
            'nombre' => 'valentina',
            'apellido1' => 'pepo',
            'apellido2' => 'pepo',
            'cedula' => '1094965678',
            'email'=> 'vale@gmail.com',
            'telefono' => 1234567,
            'ipcliente' => '180.167.0.79',
            'created_at' => now(),
            'updated_at' => now(),
            'direccion' => 'calle 52a No 69 - 24'
        ]);
        DB::table('cliente')->insert([
            'id' => 2,
            'nombre' => 'valentina2',
            'apellido1' => 'pepo2',
            'apellido2' => 'pepo2',
            'cedula' => '109654123',
            'email'=> 'vale2@gmail.com',
            'telefono' => 1234567,
            'ipcliente' => '210.1.0.58',
            'created_at' => now(),
            'updated_at' => now(),
            'direccion' => 'calle 52a No 69 - 24'
        ]);
      
    }
}
