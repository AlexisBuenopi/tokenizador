<?php

use Illuminate\Database\Seeder;

class ComercioSeeder extends Seeder
{
     /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipoComercio1 = DB::select('SELECT id FROM tipoComercio WHERE id="1" LIMIT 1');    
        $tipoComercio2 = DB::select('SELECT id FROM tipoComercio WHERE id="2" LIMIT 1');    

        DB::table('comercio')->insert([
            'id' => 1,
            'nombre' => 'valentina santa',
            'telefono' => 649684,
            'correo' => 'valen97021@gmail.com',
            'direccion' =>'osdnadamsd',
            'tipoComercio_id'=> $tipoComercio1[0]->id,
            'codVenta'=> '41',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('comercio')->insert([
            'id' => 2,
            'nombre' => 'andres123',
            'telefono' => 649684,
            'correo' => 'valen9709@gmail.com',
            'direccion' =>'osdnadamsd',
            'codVenta'=> '15641',
            'tipoComercio_id'=> $tipoComercio2[0]->id,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('comercio')->insert([
            'id' => 3,
            'nombre' => 'margarita osorio',
            'telefono' => 98956,
            'correo' => 'valen@gmail.com',
            'direccion' =>'osdnadamsd',
            'codVenta'=> '5',
            'tipoComercio_id'=> $tipoComercio1[0]->id,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
