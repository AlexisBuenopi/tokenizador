<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     * Valentina santa
     * @return void
     */
    public function run()
    {
        $this->truncateTables([
            'users',
            'comercio',
            'tipoComercio',
        ]);

        //ejecucion de seeders
           $this->call([RolesSeeder::class]);
           $this->call([UsersTableSeeder::class]);  
           $this->call([TokenSeeder::class]);
           $this->call([ClienteSeeder::class]);
           $this->call([SuscripcionSeeder::class]);
           $this->call([FranquiciaSeeder::class]);
           $this->call([TipoComercioSeeder::class]);
           $this->call([ComercioSeeder::class]);
           $this->call([TarjetaSeeder::class]);
           $this->call([DiasSeeder::class]);
           $this->call([HistorialPassSeeder::class]);       
           $this->call([PermisosSeeder::class]);
           $this->call([TokenkeySeeder::class]);
           
    }
    /**
     * Funcion que permite la eliminacion de datos 
     * en tablas multiples
     */
    public function truncateTables(array $tables)
    {
         DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
 
        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }
        //reactivar revision de llaves foraneas
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
   
}
