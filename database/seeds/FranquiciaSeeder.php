<?php

use Illuminate\Database\Seeder;

class FranquiciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('franquicia')->insert([
            'id' => 1,
            'franquicia' => 'Codensa',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('franquicia')->insert([
            'id' => 2,
            'franquicia' => 'VISA',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('franquicia')->insert([
            'id' => 3,
            'franquicia' => 'MasterCard',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('franquicia')->insert([
            'id' => 4,
            'franquicia' => 'Discover',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('franquicia')->insert([
            'id' => 5,
            'franquicia' => 'Amex',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('franquicia')->insert([
            'id' => 6,
            'franquicia' => 'Diners',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('franquicia')->insert([
            'id' => 7,
            'franquicia' => 'JCB',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('franquicia')->insert([
            'id' => 8,
            'franquicia' => 'Switch',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('franquicia')->insert([
            'id' => 9,
            'franquicia' => 'Unionpay',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('franquicia')->insert([
            'id' => 10,
            'franquicia' => 'Dankort',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('franquicia')->insert([
            'id' => 11,
            'franquicia' => 'Maestro',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('franquicia')->insert([
            'id' => 12,
            'franquicia' => 'Other',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('franquicia')->insert([
            'id' => 13,
            'franquicia' => 'Interpayment',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
