<?php

use Illuminate\Database\Seeder;

class HistorialPassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        $Useruno = DB::select('SELECT id,  password FROM users WHERE id="1" LIMIT 1');  
        $Userdos = DB::select('SELECT id, password FROM users WHERE id="2" LIMIT 1');  
       
        DB::table('historialcontrasena')->insert([
            'id' => 1,
            'password' => $Useruno[0]->password,
            'idusuario' => $Useruno[0]->id,
            'Estado'=> 1,
            'fechaCreacion'=>now(),
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('historialcontrasena')->insert([
            'id' => 2,
            'password' => $Userdos[0]->password,
            'idusuario' => $Userdos[0]->id,
            'Estado'=> 1,
            'fechaCreacion'=>now(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

    }
}
