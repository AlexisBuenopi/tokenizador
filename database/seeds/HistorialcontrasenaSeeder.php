<?php

use Illuminate\Database\Seeder;

class HistorialcontrasenaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Usuarioid = DB::select('SELECT id FROM users WHERE id="1" LIMIT 1');  

        DB::table('historialcontrasena')->insert([
            'id' => 1,
            'password' => '$2y$10$v4/TJJ842AoFZS3dfrSrw.hUc4t586whjpaEGPGpjizHSfZGiLrJS',
            'Estado' => 1,
            'fechaCreacion' => now(),
            'created_at' => now(),
            'updated_at' => now(),
            'idusuario' => $Usuarioid[0]->id
        ]);
    }
}
