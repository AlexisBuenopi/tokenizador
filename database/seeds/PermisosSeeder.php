<?php

use Illuminate\Database\Seeder;

class PermisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permisos')->insert([
            'id' => 1,
            'Descripcion' => 'Puede crear el token',
            'Estado' => 1,            
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
