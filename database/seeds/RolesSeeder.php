<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rol')->insert([
            'id' => 1,
            'nombre' => 'SuperAdministrador',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('rol')->insert([
            'id' => 2,
            'nombre' => 'Administrador',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('rol')->insert([
            'id' => 3,
            'nombre' => 'Cliente',
            'created_at' => now(),
            'updated_at' => now()
        ]);

    


    }
}
