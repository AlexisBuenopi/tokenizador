<?php

use Illuminate\Database\Seeder;

class SuscripcionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('suscripcion')->insert([
            'id' => 1,
            'tipoSuscripcion' => 'Pago unico',
            'descripcion'=> 'fghrh',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('suscripcion')->insert([
            'id' => 2,
            'tipoSuscripcion' => 'Pago mensual',
            'descripcion'=> 'fghrh',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('suscripcion')->insert([
            'id' => 3,
            'tipoSuscripcion' => 'Inactivo',
            'descripcion'=> 'fghrh',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
