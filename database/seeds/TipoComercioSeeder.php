<?php

use Illuminate\Database\Seeder;

class TipoComercioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipoComercio')->insert([
            'id' => 1,
            'descripcion' => 'Gateway',
            'created_at' => now(),
            'updated_at' => now()
        ]);
        DB::table('tipoComercio')->insert([
            'id' => 2,
            'descripcion' => 'Agregador',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}
