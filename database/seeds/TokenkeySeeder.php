<?php

use Illuminate\Database\Seeder;

class TokenkeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('token_key')->insert([
            'id' => 1,
            'tokenkey' => '9a7e9d94-ae01-49fc-be55-cc6fe2c7d671'            
        ]);
    }
}
