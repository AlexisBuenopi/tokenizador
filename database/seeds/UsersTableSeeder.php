<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'jabueno89@misena.edu.co',
            'cargo' => 'Admin',
            'email_verified_at' => now(),
            'password' => Hash::make('P_inteligentes_t0k3n'),
            'codigo_seguridad_id' => null,
            'created_at' => now(),
            'updated_at' => now(),
            'Fecha_Ingreso' => now(),
            'Estado' => 1,
            'rol_id' => 2

        ]);
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'valen970211@gmail.com',
            'cargo' => 'Admin',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'codigo_seguridad_id' => null,
            'created_at' => now(),
            'updated_at' => now(),
            'Fecha_Ingreso' => now(),
            'Estado' => 1,
            'rol_id' => 1
        ]);
    }
}
