@extends('layouts.app', ['class' => 'off-canvas-sidebar', 'activePage' => 'login', 'title' => __('Bienvenido - Tokenizador Pagos Inteligentes')])

@section('content')
<div class="container" style="height: auto;">
  <div class="row align-items-center">
    <div class="col-md-9 ml-auto mr-auto mb-3 text-center">
      <h3>{{ __('Tokenizador') }} </h3>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
      <form class="form" method="POST" action="{{ route('login') }}">
        @csrf
            
        <div class="card card-login card-hidden mb-3">
          <div class="card-header card-header-primary text-center">
            <h4 class="card-title"><strong>{{ __('Login') }}</strong></h4>
            <div class="social-line">

            </div>
          </div>
          <div class="card-body">
            <p class="card-description text-center">{{ __('Ingresa Usuario ') }} <strong>XXXXXXXXXXX@PagosInteligentes.com</strong> {{ __(' Y Password ') }} </p>
            <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">email</i>
                  </span>
                </div>
                <input type="email" name="email" class="form-control" placeholder="{{ __('Email...') }}" value="{{ $email ?? ''}}" required>
              </div>
              @if ($errors->has('email'))
                <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                  <strong>{{ $errors->first('email') }}</strong>
                </div>
              @endif
            </div>
            <div class="bmd-form-group{{ $errors->has('password') ? ' has-danger' : '' }} mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">lock_outline</i>
                  </span>
                </div>
                <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('Password...') }}" value="{{ $password ?? ''}}"required>
              </div>
              @if ($errors->has('password'))
                <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                  <strong>{{ $errors->first('password') }}</strong>
                </div>
              @endif
            </div>
            {{-- verifyCode--}}
            @if($showCode ?? '' == true)
            <div class="bmd-form-group{{ $errors->has('code') ? ' has-danger' : '' }} mt-3">
              <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                  <strong>Ingresa el codigo enviado a tu correo</strong>
              </div>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="material-icons">format_list_numbered</i>
                  </span>
                </div>
                <input type="text" name="code" id="code" class="form-control" placeholder="{{ __('code...') }}" value="{{ $code ?? ''}}" >
              </div>
            </div>
          @endif
          @if ($codeInvalid ?? '' == true)
                <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                  <strong> El codigo no es valido</strong>
                </div>
          @endif
          @if ($msg ?? '' == true)
                <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                  <strong>Ya se envio un correo con codigo de verificacion</strong>
                </div>
          @endif
            {{-- End verifyCode --}}
            <div class="form-check mr-auto ml-3 mt-3">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember me') }}
                <span class="form-check-sign">
                  <span class="check"></span>
                </span>
              </label>
            </div>
          </div>
          <div class="card-footer justify-content-center">
            <button type="submit" class="btn btn-primary btn-link btn-lg">{{ __('Iniciar sesion') }}</button>
          </div>
        </div>
      </form>
      <div class="row">
        <div class="col-6">
            @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}" class="text-light">
                    <small>{{ __('Olvidaste la contrasena?') }}</small>
                </a>
            @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
