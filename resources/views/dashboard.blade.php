@extends('layouts.app', ['activePage' => 'Servicio Amazon-AWS', 'titlePage' => __('Registro de ususario')])

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="container" style="height: auto;">
    @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif


      <div class="row align-items-center">
        <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
          <form class="form" method="post" action="{{ route('createkey') }}">
            @csrf
            @method('post')
            <div class="card card-login card-hidden mb-3">
              <div class="card-header card-header-primary text-center">
                <h4 class="card-title"><strong>{{ __('Generate key') }}</strong></h4>
                <div class="social-line">
                </div>
              </div>

           
              <div class="card-body ">
                <p class="card-description text-center">{{ __('Ingresa los datos Solicitados') }}</p>
                <div class="bmd-form-group{{ $errors->has('key') ? ' has-danger' : '' }}">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="material-icons">lock</i>
                      </span>
                    </div>
                    <input type="text" name="key" class="form-control" placeholder="{{ __('Key..') }}" value="{{ $key ?? ''}}" required>
                  </div>
                  @if ($errors->has('key'))
                    <div id="Key-error" class="error text-danger pl-3" for="key" style="display: block;">
                      <strong>{{ $errors->first('key') }}</strong>
                    </div>
                  @endif
                </div>
                <div class="bmd-form-group{{ $errors->has('KMS_key') ? ' has-danger' : '' }}">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="material-icons">admin_panel_settings</i>
                      </span>
                    </div>
                    <input type="text" name="KMS_key" class="form-control" placeholder="{{ __('KMS-key...') }}" value="{{ $KMS_key ?? ''}}" required>
                  </div>                   
              </div>
                <div class="bmd-form-group{{ $errors->has('key_value') ? ' has-danger' : '' }}">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="material-icons">line_style</i>
                      </span>
                    </div>
                    <input type="text" id="key_value" name="key_value" class="form-control" placeholder="{{ __('Parametro...') }}" value="{{ $key_value ?? ''}}" >
                  </div>
                  @if ($errors->has('key_value'))
                    <div id="key_value-error" class="error text-danger pl-3" for="cargo" style="display: block;">
                      <strong>{{ $errors->first('key_value') }}</strong>
                    </div>
                  @endif
                 </div> 
             
              <div class="bmd-form-group{{ $errors->has('Esdek') ? ' has-danger' : '' }}">
                
               <div class="input-group" style="margin-left: 9px;"> 
                  <div class="input-group-prepend">
                   <span class="input-group-text">
                      <input type="checkbox" name="Esdek" id="Esdek" >  
                      </span>    
                  </div>            
                      <label  class="form-check-label form-control" for="Esdek">Es DEK</label>
                </div>
              </div>
             
              <div class="card-footer justify-content-center">
                <button type="submit" class="btn btn-primary btn-link btn-lg" data-toggle="modal" data-target="#exampleModal">{{ __('Generar') }}</button>
              </div>
          
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@push('js')
<script>
$(document).ready(function() {

 var ckeck = $('#Esdek').is(':checked');
 console.log(ckeck);
  if(ckeck == false) {
    $('#key_value').prop("required", true);
}

 $( "#Esdek" ).click(function() {
  if( $('#Esdek').is(':checked')){
  $('#key_value').removeAttr("required");
  $('#key_value').prop("readonly", true);
  $('#key_value').val("");


 }else{
  $('#key_value').prop("required", true);
  $('#key_value').prop("readonly", false);

 }

});





    
  });
</script>
@endpush