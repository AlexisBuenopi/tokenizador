<div class="sidebar" data-color="orange" data-background-color="white"
    data-image="{{ asset('material') }}/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">
        <a href="https://creative-tim.com/" class="simple-text logo-normal">
            {{ __('Tokenizador') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">



            @if(Auth::user()->rol_id != 3)
        <li class="nav-item {{ ( $activePage == 'user-management') ? 'active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#laravelExample2" aria-expanded="false">
				<i class="material-icons">lock</i>
                    <p>{{ __('Llave') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse show" id="laravelExample2">
                <ul class="nav">

                        <li class="nav-item{{ $activePage == 'Servicio Amazon-AWS' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('home') }}">
                                <span class="sidebar-mini"> CL </span>
                                <span class="sidebar-normal"> {{ __('Crear llave') }} </span>
                            </a>
                        </li>

                        <li class="nav-item{{ $activePage == 'Buscar llave' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('listarllave') }}">
                                <span class="sidebar-mini"> LL </span>
                                <span class="sidebar-normal"> {{ __('Listar llaves') }} </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>


            <li class="nav-item{{ $activePage == 'register' ? ' active' : '' }}">
                <a href="{{ route('userregister') }}" class="nav-link">
                    <i class="material-icons">person_add</i> {{ __('Registro') }}
                </a>
            </li>


            <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('profile.edit') }}">
                   <i class="material-icons">person_add</i> {{  __('User profile')  }}
                </a>
            </li>
            @endif
            <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
                <a class="nav-link" data-toggle="collapse" href="#laravelExample" aria-expanded="false">
                    <i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>
                    <p>{{ __('Tokenizador') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse show" id="laravelExample">
                <ul class="nav">
                        <li class="nav-item{{ $activePage == 'buscarToken' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('BuscarToken') }}">
                                <span class="sidebar-mini"> BT </span>
                                <span class="sidebar-normal">{{ __('Buscar token') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'crear' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('crearToken') }}">
                                <span class="sidebar-mini"> CT </span>
                                <span class="sidebar-normal"> {{ __('Crear token') }} </span>
                            </a>
                        </li>
                        <li style="display: none;" class="nav-item{{ $activePage == 'cargar' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('cargarToken') }}">
                                <span class="sidebar-mini"> CT </span>
                                <span class="sidebar-normal"> {{ __('Cargar token') }} </span>
                            </a>
                        </li>
                        <li class="nav-item{{ $activePage == 'Lista de tokens' ? ' active' : '' }}">
                            <a class="nav-link" href="{{ route('listar') }}">
                                <span class="sidebar-mini"> CT </span>
                                <span class="sidebar-normal"> {{ __('Listar tokens') }} </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>
