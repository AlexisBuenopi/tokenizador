@extends('layouts.app', ['activePage' => 'Buscar llave', 'titlePage' => __('Buscar Llave')])

@section('content')
{{-- Modal Inicio--}}
<div id="modalEditar" class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar llave</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('updatellave') }}"  class="form-horizontal">
            @csrf
            <div class="modal-body">
           <div class="bmd-form-group{{ $errors->has('key') ? ' has-danger' : '' }}">
                
                    <div class="input-group">
                        <div class="input-group-prepend">
                        <span class="input-group-text">
                            <i class="material-icons">lock</i>
                        </span>
                        </div>
                        <input type="text" name="key"  id="key"   class="form-control" placeholder="{{ __('Key..') }}" value="{{ old('Key') }}" readonly="readonly" required>
                    </div>
                </div>
                <div class="bmd-form-group{{ $errors->has('key_value') ? ' has-danger' : '' }}">                 
                <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="material-icons">line_style</i>
                      </span>
                    </div>
                    <input type="text" id="key_value" name="key_value" class="form-control" placeholder="{{ __('Key Value...') }}" value="{{ old('key_value') }}"  requiered>
                  </div>
                </div>
                <div class="bmd-form-group{{ $errors->has('KMS_key') ? ' has-danger' : '' }}">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="material-icons">admin_panel_settings</i>
                      </span>
                    </div>
                    <input type="text" name="KMS_key" class="form-control" placeholder="{{ __('KMS-key...') }}" value="{{ old('KMS_key') }}" required>
                  </div>                   
              </div>
      
            </div>
            <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
      </form>
        </div>
    </div>
</div>
{{-- Modal Fin--}}


<div class="content">
    <div class="container-fluid">
    @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif
        <div class="row">
            <form class="col-lg-12 navbar-form content">
                <span class="bmd-form-group">
                    <div class="input-group no-border">
             

            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">{{ __('Llaves') }}</h4>
                    </div>
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>
                                            Key
                                        </th>
                                        <th>
                                            Fecha creacion
                                        </th>
                                        <th>
                                            Estado
                                        </th>
                                        <th>
                                            Tipo
                                        </th>                                     
                                        <th>
                                            Accion
                                        </th>  
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($buscarLlaves))
                                    @foreach ($buscarLlaves as $listaLlave)
                                    <tr>
                                        <td>
                                            {{ $listaLlave -> parameterkey }}
                                        </td>
                                        <td>
                                            {{ $listaLlave -> created_at}}
                                        </td>
                                        <td >
                                        {{ $listaLlave->Estado== 1 ? "Activo" : 'Inactivo'}}
                                        </td>
                                        <td >
                                        {{ $listaLlave->Esdek== 1 ? "DEK" : 'No DEK'}}
                                        </td>
                                        <td class="td-actions text-right">
                                            <button type="button"  rel="tooltip" title=""
                                                data-toggle="modal" data-target="#modalEditar"
                                                class="btn btn-primary btn-link btn-lg"     onclick="Desplegar({{ $listaLlave->id }},'{{ $listaLlave->parameterkey }}' )"
                                                data-original-title="Editar llave {{ $listaLlave -> parameterkey }} " aria-describedby="tooltip917679">
                                                <i class="large material-icons">edit</i>
                                                <div class="ripple-container"></div>
                                            </button>
                                            <label class="switch" >
                                            <input type="checkbox" id="Estado{{ $listaLlave->id }}"  onclick="CambiarEstado({{ $listaLlave->id }})"   {{ $listaLlave->Estado== 1 ? "checked" : ''}}  >
                                            <span class="slider round"></span>
                                            </label>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
$(document).ready(function() {

});


function Desplegar(id, campo){
    $('#key_value').val('');
    $('#key').val(campo);
    $.ajax({
            type: 'get',            
            url: "{{ route('getidlllave') }}",
            data: {
                'idllave': id
            },
            success: function(resultado) {
              
                if(resultado.Esdek){
                    $('#key_value').prop('readonly', true);
                    $('#key_value').removeAttr('required');
                }else{
                    $('#key_value').prop('readonly', false);

                    $.ajax({
                        type: 'get',
                        url: "{!!URL::to('getidparameter')!!}",
                         data: {
                             'parameterkey': resultado.parameterkey
                        },
                         success: function(resultado) {                           
                            $('#key_value').val(resultado);
                         }
                                    });
                 }          
                $('#modalEditar').modal('show');

                


            }
        });

}  


function CambiarEstado(id){

    var check = $('#Estado'+id).is(':checked');
console.log('estado check: '+check);
    $.ajax({
        type: 'get',
        url: "{{ route('updateestadollave') }}",
        data: {
            'Estado': check,
            'idparameter': id
            },
        success: function(resultado) {                           
            location.reload();
         } 
        });
    }







    

</script>
@endpush