@extends('layouts.app', ['activePage' => 'buscar', 'titlePage' => __('Buscar token')])

@section('content')
{{-- Modal Inicio--}}
<div id="modalEliminar" class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Creacion de token</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseas aliminar el token?
            </div>
            <form method="get" action="{{ route('eliminarToken') }}" autocomplete="off" class="form-horizontal">
                @csrf
                @method('get')
                <input type="hidden" value="" name="id_token" id="id_token">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Aceptar</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Modal Fin--}}
{{-- Modal Inicio--}}
<div id="modalEditar" class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Creacion de token</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Deseas editar el token?
            </div>
            <form method="post" action="{{ route('editarToken') }}" autocomplete="off" class="form-horizontal">
                @csrf
                @method('put')
                <input type="hidden" value="" name="id_token" id="id_token2">
                <input type="hidden" value="" name="fecha_id" id="id_fecha">
                <input type="hidden" value="" name="estado_id" id="id_estado">
                <input type="hidden" value="" name="suscripcion_id" id="id_suscripcion">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Aceptar</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Modal Fin--}}

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <form class="col-lg-12 navbar-form content">
                <span class="bmd-form-group">
                    <div class="input-group no-border">
                        <form method="get" action="{{ route('BuscarToken') }}" autocomplete="off"
                            class="form-horizontal">
                            @csrf
                            @method('get')
                            <div>
                                <label class="">Nombre:</label>
                                <div class="form-group bmd-form-group">
                                <input type="text" name="text" value="{{ $token ?? '' }}" class="form-control"
                                placeholder="Buscar por nombre...">
                                </div>
                            </div>
                            <div>
                                <label class="">Fecha inicial:</label>
                                <div class="form-group bmd-form-group">
                                <input type="date" name="inicio" value="{{ $f1 ?? '' }}" class="form-control"
                                placeholder="Buscar por nombre...">
                                </div>
                            </div>
                            <div>
                                <label class="">Fecha final:</label>
                                <div class="form-group bmd-form-group">
                                <input type="date" name="fin" value="{{ $f2 ?? '' }}" class="form-control"
                                placeholder="Buscar por nombre fecha...">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>
                    </div>
                </span>
            </form>
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">{{ __('Cargar token') }}</h4>
                    </div>
                    <div class="card-body ">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>
                                            Nombre
                                        </th>
                                        <th>
                                            Fecha creacion
                                        </th>
                                        <th>
                                            Telefono
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Token
                                        </th>
                                        <th>
                                            Franquicia
                                        </th>
                                        <th>
                                            Nombre Tarjeta
                                        </th>
                                        <th>
                                            Fecha venc
                                        </th>
                                        <th>
                                            Estado
                                        </th>

                                        <th>
                                            Gestionar
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($buscarTokens))
                                    @foreach ($buscarTokens as $listaToken)
                                    <tr>
                                        <td>
                                            {{ $listaToken -> nombre }}
                                        </td>
                                        <td>
                                            {{ $listaToken -> created_at}}
                                        </td>
                                        <td>
                                            {{ $listaToken -> telefono }}
                                        </td>
                                        <td>
                                            {{ $listaToken -> email }}
                                        </td>
                                        <td>
                                            {{ $listaToken -> token }}
                                        </td>
                                        <td>
                                            {{ $listaToken -> franquicia }}
                                        </td>
                                        <td>
                                            {{ $listaToken -> tarjeta }}
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <input class="form-control" name="fecha_vencimiento"
                                                    id="fecha-{{$listaToken->id}}" type="text"
                                                    placeholder="{{ __('Name') }}"
                                                    value="{{ $listaToken -> fechaVencimiento }}" required="true"
                                                    aria-required="true" />
                                            </div>
                                        </td>
                                        <td class="text-right">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input id="estado-{{$listaToken->id}}" name="estado"
                                                        class="form-check-input" type="checkbox" {{ $listaToken->estado
                                                        == 1 ? "checked" : ''}}>
                                                    <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                        </td>
                                        <td class="td-actions text-right">
                                            <button style="display: none;" type="button" data-id="{{ $listaToken->id }}" rel="tooltip" title=""
                                                data-toggle="modal" data-target="#modalEditar"
                                                class="btn btn-primary btn-link btn-sm"
                                                data-original-title="Editar token" aria-describedby="tooltip917679">
                                                <i class="material-icons">edit</i>
                                                <div class="ripple-container"></div>
                                            </button>
                                            <button data-id="{{ $listaToken->id }}" data-toggle="modal"
                                                data-target="#modalEliminar" type="button" rel="tooltip" title=""
                                                class="btn btn-danger btn-link btn-sm" data-original-title="Eliminar">
                                                <input type="hidden" value="{{$listaToken->id}}" name="id_token"
                                                    id="tokenid">
                                                <i class="material-icons">close</i>
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
$(document).ready(function() {

    $('#modalEliminar').on("show.bs.modal", function(e) {
        console.log('abrir modal bs', e.relatedTarget);
        $("#id_token").val($(e.relatedTarget).data('id'));
    });
    $('#modalEditar').on("show.bs.modal", function(e) {
        console.log('abrir modal bs', e.relatedTarget);
        $("#id_token2").val($(e.relatedTarget).data('id'));
        console.log('hola');

        var id = $('#tokenid').val();
        var fecha = $('#fecha-' + id).val();
        var suscripcion = $('#suscripcion-' + id).val();
        var estado = $('#estado-' + id).prop('checked');
        console.log('fecha', fecha);

        $('#id_fecha').val(fecha);
        $('#id_suscripcion').val(suscripcion);
        $('#id_estado').val(estado);
    });
});
</script>
@endpush
