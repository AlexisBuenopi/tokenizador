@extends('layouts.app', ['activePage' => 'cargar', 'titlePage' => __('Cargar token')])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <form method="get" action="{{ route('cargarTokens') }}" class="col-md-6 navbar-form content">
                @csrf
                @method('get')
                <span class="bmd-form-group">
                    <div class="input-group no-border">
                        <input id="busqueda" name="busqueda" type="text" value="{{$busqueda ?? ''}}" class="form-control" placeholder="Buscar por nombre..." required>
                        <button id="search" type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i>
                            <div class="ripple-container"></div>
                        </button>
                    </div>
                </span>
            </form>
            <div class="col-md-12">
                <div class="card ">
                    <div class="card-header card-header-primary">
                        <h6 class="card-title">{{ __('Cargar token') }}</h4>
                    </div>
                    @if ($msg ?? '' == true)
                        <div id="password-error" class="error text-danger pl-3" for="password" style="display: block;">
                          <strong>no hay datos existentes para la busqueda </strong>
                        </div>
                    @endif
                    <div class="card-body ">
                    @if(isset($tokens))
                        <div class="row">
                            <div class="col-md-6">
                                <label class="">{{ __('Nombre tarjeta:') }}</label>
                                <div class="form-group">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" name="inicio" value="{{$tokens->nombre}}" class="form-control"
                                            placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="">{{ __('Token:') }}</label>
                                <div class="form-group">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" name="inicio" value="{{$tokens->token}}" class="form-control"
                                            placeholder="Buscar por nombre...">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="">{{ __('Fecha vencimiento:') }}</label>
                                <div class="form-group">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" name="inicio" value="{{$tokens->fechaVencimiento}}" class="form-control"
                                            placeholder="Buscar por nombre...">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="">{{ __('Franquicia:') }}</label>
                                <div class="form-group">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" name="inicio" value="{{$tokens->franquicia}}" class="form-control"
                                            placeholder="Buscar por nombre...">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="">{{ __('Comercio:') }}</label>
                                <div class="form-group">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" name="inicio" value="{{$tokens->nombre}}" class="form-control"
                                            placeholder="Buscar por nombre...">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="">{{ __('Tipo Suscripcion:') }}</label>
                                <div class="form-group">
                                    <div class="form-group bmd-form-group">
                                        <input type="text" name="inicio" value="{{$tokens->TipoSuscripcion}}" class="form-control"
                                            placeholder="Buscar por nombre...">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection