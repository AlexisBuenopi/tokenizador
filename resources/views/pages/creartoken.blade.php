@extends('layouts.app', ['activePage' => 'crear', 'titlePage' => __('Crear token')])

@section('content')
{{-- Modal Inicio--}}

<div id="myModal" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Creacion de token</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Tarjeta/Token creado con exito
            </div>
            {{-- 
            <div class="modal-footer">
                <button id="close" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
           --}}
        </div>
    </div>
</div>
{{-- Modal Fin--}}
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form id="form" method="post" action="{{ route('crearToken') }}" autocomplete="off"
                    class="form-horizontal">
                    @csrf
                    @method('post')
                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('Crear token') }}</h4>
                        </div>
                        <div class="card-body ">
                            @if (session('status'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('status') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif

                            @if (session('ErroresProcedimiento'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <i class="material-icons">close</i>
                                        </button>
                                        <span>{{ session('ErroresProcedimiento') }}</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if($errors->any())
                            <input id="error" type="hidden" name="error">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Formulario incorrecto!</strong> Verifica los campos
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif

                            @if ($errors->has('Llave_cifrado'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-name">{{ $errors->first('name') }}</span>
                                        @endif

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="">{{ __('Numero de tarjeta:') }}</label>
                                    <div class="form-group">
                                        <input minlength="15" maxlength="16" class="form-control" name="numero_tarjeta"
                                            id="numero_tarjeta" type="text" placeholder="{{ __('Numero tarjeta') }}"
                                            value="{{ old('numero_tarjeta') }}" required="true" aria-required="true" />
                                        @if ($errors->has('name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-name">{{ $errors->first('name') }}</span>
                                        @endif
                                        <img width="50" height="50" src="" alt="" id="card-img">
                                        <input id="franchise" type="hidden" name="franchise" value="{{ old('franchise') }}" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="">{{ __('Fecha de expiracion:') }}</label>
                                    <div class="form-group">
                                        <input class="form-control" name="fecha_exp" step="1" id="fecha_exp"
                                            type="month" placeholder="{{ __('Mes:') }}" value="{{ old('fecha_exp') }}"
                                            required="true" min="2020-01" max="2060-12" aria-required="true" />
                                        @if ($errors->has('name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-name">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="">{{ __('Nombre de la tarjeta:') }}</label>
                                    <div class="form-group">
                                        <input class="form-control" name="nombre_tarjeta" id="nombre_tarjeta"
                                            type="text" placeholder="{{ __('Nombre tarjeta') }}"
                                            value="{{ old('nombre_tarjeta') }}" required="true" aria-required="true" />
                                    </div>
                                </div>
                                </div>
                            </div >
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="col-md-6">{{ __('Valor a pagar:') }}</label>
                                    <div class="form-group">
                                    <div class="col-md-6">
                                        <input class="form-control " name="valor_pago" id="valor_pago" type="number"
                                            placeholder="{{ __('Valor') }}" value="{{ old('valor_pago') }}"
                                            required="true" aria-required="true" />
                                            </div>
                                        @if ($errors->has('name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-name"s>{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <label class=" ">{{ __('Referencia(opcional):') }}</label>
                                    <div class="form-group">
                                        <input class="form-control" name="referencia" id="referencia" type="text"
                                            placeholder="{{ __('Codigo') }}" value="{{ old('referencia') }}"
                                            aria-required="true" />
                                        @if ($errors->has('name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-name">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" ">{{ __('Tipo suscripcion:') }}</label>
                                        <select name="suscripcion" class="form-control" data-style="btn btn-link"
                                            id="exampleFormControlSelect1">
                                            <option value="1">Pago unico</option>

                                            <option value="2">Pago mensual</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="colum">
                                <div class="col-md-6">
                                    <label class="">{{ __('Comercio:') }}</label>
                                    <div class="form-group">
                                        <input class="form-control" name="comercio" id="comercio" type="text"
                                            placeholder="{{ __('Buscar comercio..') }}" value="{{ old('comercio') }}"
                                            required="true" aria-required="true" />
                                        <ul id="myList">
                                        </ul>
                                        <input type="hidden" name="busqueda" id="busqueda" type="text">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="">{{ __('Programar pagos automaticos:dia') }}</label>
                                    <div class="form-group">
                                        <input type="date" class="form-control" name="fecha_pagos" id="fecha_pagos"
                                            type="text" placeholder="{{ __('Dia:') }}" value="{{ old('fecha_pagos') }}"
                                            required="true" aria-required="true" />
                                        @if ($errors->has('name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-name">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-danger" role="alert">
                                <h4 style="color:white;" class="card-title">{{ __('Datos cliente') }}</h4>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="">{{ __('nombre:') }}</label>
                                    <div class="form-group">
                                        <input class="form-control" name="nombre_cliente" id="nombre" type="text"
                                            placeholder="Nombre cliente" value="{{ old('nombre_cliente') }}" required="true"
                                            aria-required="true" />
                                        <ul id="myList">
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="">{{ __('Primer apellido:') }}</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="apellido1_cliente"
                                            id="fecha_pagos" type="text" placeholder="{{ __('Apellido 1:') }}"
                                            value="{{ old('apellido1_cliente') }}" required="true"
                                            aria-required="true" />
                                        @if ($errors->has('name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-name">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="">{{ __('Segundo apellido:') }}</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="apellido2_cliente"
                                            id="fecha_pagos" type="text" placeholder="{{ __('Apellido 2:') }}"
                                            value="{{ old('apellido2_cliente') }}" aria-required="true" />
                                        @if ($errors->has('name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-name">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="">{{ __('Email:') }}</label>
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email_cliente" id="fecha_pagos"
                                            type="text" placeholder="Email" value="{{ old('email_cliente') }}"
                                            aria-required="true" />
                                        @if ($errors->has('name'))
                                        <span id="name-error" class="error text-danger"
                                            for="input-name">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="">{{ __('Cedula:') }}</label>
                                    <div class="form-group">
                                        <input class="form-control" name="cedula_cliente" id="cedula" type="text"
                                            placeholder="Cedula" value="{{ old('cedula_cliente') }}" required="true"
                                            aria-required="true" />
                                        <ul id="myList">
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="">{{ __('Telefono:') }}</label>
                                    <div class="form-group">
                                        <input class="form-control" name="telefono_cliente" id="telefono" type="number"
                                            placeholder="Telefono" value="{{ old('telefono_cliente') }}" required="true"
                                            aria-required="true" />
                                        <ul id="myList">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <button id="submit" data-toggle="modal" data-target="#exampleModal"
                                class="btn btn-primary">{{ __('Crear token') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
$(document).ready(function() {
    var checkCreditCard = function() {
        var number = $('#numero_tarjeta').val();
        
        $.ajax({
            type: 'get',
            url: "{!!URL::to('creditCardReveal')!!}",
            data: {
                'number': number
            },
            success: function(result) {
        
                $('#prueba').val(result.result);
                let map = {
                    "VISA": "{{ asset('material') }}/franchise/visa.png",
                    "MasterCard": "{{ asset('material') }}/franchise/unnamed.jpg",
                    "Amex": "{{ asset('material') }}/franchise/american-express.png",
                    "Diners": "{{ asset('material') }}/franchise/diners-club.png",
                    "JCB": "{{ asset('material') }}/franchise/jcb.png",
                    "Dankort": "{{ asset('material') }}/franchise/240x0w.png",
                    "Maestro": "{{ asset('material') }}/franchise/descarga.png",
                    "Forbrugsforeningen": "{{ asset('material') }}/franchise/1200x630wa.png",
                    " ": " "
                };
                Object.keys(map).forEach(brand => {
                    console.log("numero brand "+brand);
                    console.log("numero tarjeta "+result.result);
                    if (brand === result.result) {
                        console.log("numero tarjeta "+result.result);
                        $('#card-img').attr('src', map[brand])
                        $('#franchise').val(brand);
                    }
                })
                console.log(result);
            },
            error: function() {

            }
        });
    };
   // llamarlo cuando cargue old values
   checkCreditCard();
   // verificar cambios en el input
   $('#numero_tarjeta').on('input', checkCreditCard);

   var checkComercio = function() {
        var text = $('#comercio').val();
        console.log('comercio');
        $.ajax({
            type: 'get',
            url: "{!!URL::to('foundComercio')!!}",
            data: {
                'text': text
            },
            success: function(listaComercio) {
                console.log(listaComercio)
                $('#busqueda').val(listaComercio.descripcion);
                if (Array.isArray(listaComercio)) {
                    $('#myList').empty();
                    listaComercio.forEach(element => {
                        var comp = $(`<li id="${element.id}">
                                ${element.nombre} - ${element.codVenta} - ${element.descripcion}
                            </li>`);
                        comp.on('click', (event) => {
                            console.log(event);
                            $('#comercio').val(event.target.innerText)
                            $('#busqueda').val(event.target.id);
                        })
                        $('#myList').append(
                            comp
                        );
                    });
                }
            },
            error: function() {
                console.log('error')
            }
        });
    };
    checkComercio();
    //Busqueda de comerio
    $('#comercio').on('input', checkComercio);

    $('form').on('submit', function(e) {
        if($('#error').val() == ' '){
             $('#myModal').modal('show');
        }
    });
    $("#close").on('click', function() {
        $('#form').submit(function() {
            console.log('importante')
        });
        console.log('hola')
    });

   

});
</script>
@endpush