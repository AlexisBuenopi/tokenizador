@extends('layouts.app', ['activePage' => 'listar', 'titlePage' => __('Lista de tokens')])

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Tokens</h4>
                        <p class="card-category"> Here you can manage users</p>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 text-right">
                                <a href="{{ route('crearToken') }}" class="btn btn-sm btn-primary">Add Token</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>
                                            Nombre Cliente
                                        </th>
                                        <th>
                                            Token
                                        </th>
                                        <th>
                                            Franquicia
                                        </th>
                                        <th>
                                            Nombre Tarjeta
                                        </th>
                                        <th>
                                            Fecha venc
                                        </th>                                      
                                        <th>
                                            Estado
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($listaTokens))
                                    @foreach ($listaTokens as $listaToken)
                                    <tr>
                                        <td>{{ $listaToken -> nombre }}  {{ $listaToken -> apellido1 }} </td>
                                        <td>{{ $listaToken -> token }}</td>
                                        <td>{{ $listaToken -> franquicia }}</td>
                                        <td>{{ $listaToken -> tarjeta}}</td>
                                        <td>{{ $listaToken -> fechaVencimiento }}</td>                                    
                                        @if($listaToken -> estado =='1')
                                        <td>ACTIVO</td>
                                        @else
                                        <td>INACTIVO</td>
                                        @endif
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            {!! $listaTokens->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
