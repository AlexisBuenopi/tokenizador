@extends('layouts.app', ['activePage' => 'user-register', 'titlePage' => __('Registro de ususario')])

@section('content')
{{-- Modal Inicio--}}
<div id="myModal" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Creacion de token</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Usuario creado con exito
            </div>
            {{-- 
            <div class="modal-footer">
                <button id="close" type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
           --}}
        </div>
    </div>
</div>
{{-- Modal Fin--}}
<div class="content">
  <div class="container-fluid">
    <div class="container" style="height: auto;">
      <div class="row align-items-center">
        <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
          <form class="form" method="post" action="{{ route('userregister') }}">
            @csrf
            @method('post')
            <div class="card card-login card-hidden mb-3">
              <div class="card-header card-header-primary text-center">
                <h4 class="card-title"><strong>{{ __('Registro') }}</strong></h4>
                <div class="social-line">
                </div>
              </div>
              <div class="card-body ">
                <p class="card-description text-center">{{ __('Ingresa los datos personales') }}</p>
                <div class="bmd-form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="material-icons">face</i>
                      </span>
                    </div>
                    <input type="text" name="name" class="form-control" placeholder="{{ __('Nombre...') }}" value="{{ old('name') }}" required>
                  </div>
                  @if ($errors->has('name'))
                    <div id="name-error" class="error text-danger pl-3" for="name" style="display: block;">
                      <strong>{{ $errors->first('nombre') }}</strong>
                    </div>
                  @endif
                </div>
                <div class="bmd-form-group{{ $errors->has('cargo') ? ' has-danger' : '' }}">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="material-icons">card_travel</i>
                      </span>
                    </div>
                    <input type="text" name="cargo" class="form-control" placeholder="{{ __('Cargo...') }}" value="{{ old('cargo') }}" required>
                  </div>
                  @if ($errors->has('cargo'))
                    <div id="cargo-error" class="error text-danger pl-3" for="cargo" style="display: block;">
                      <strong>{{ $errors->first('cargo') }}</strong>
                    </div>
                  @endif
                </div>
                <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} mt-3">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">email</i>
                      </span>
                    </div>
                    <input type="email" name="email" class="form-control" placeholder="{{ __('Email...') }}" value="{{ old('email') }}" required>
                  </div>
                  @if ($errors->has('email'))
                    <div id="email-error" class="error text-danger pl-3" for="email" style="display: block;">
                      <strong>{{ $errors->first('email') }}</strong>
                    </div>
                  @endif
                </div>
                <div class="bmd-form-group{{ $errors->has('email') ? ' has-danger' : '' }} mt-3">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="material-icons">people_alt</i>
                      </span>
                    </div>
                    <select  id="rol" name="rol" class="form-control"   required>
                   <option value=""> - Seleccione un rol -   </option>
                   @foreach($rols as $key => $value)
                <option value="{{$key}}">{{$value}}</option>
              @endforeach
                    </select>
                  </div>
                  @if ($errors->has('email'))
                    <div id="rol-error" class="error text-danger pl-3" for="rol" style="display: block;">
                      <strong>{{ $errors->first('rol') }}</strong>
                    </div>
                  @endif
                </div>
                
              </div>
              <div class="card-footer justify-content-center">
                <button type="submit" class="btn btn-primary btn-link btn-lg" data-toggle="modal" data-target="#exampleModal">{{ __('Crear cuenta') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
@push('js')
<script>
$(document).ready(function() {

    
  });
</script>
@endpush