<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;





Route::get('Tokenizador/descrifrar/{email}/{token}', 'TokenController@GetTarjetaDescrifrada');
Route::resource('Tokenizador', 'TokenController');


Route::get('Tokenizador/TarjetasCliente/{email}', 'TokenController@show');
Route::resource('Tokenizador', 'TokenController');


//localhost.com/api/Tokenizador/CrearToken
Route::post('Tokenizador/CrearToken', 'TokenController@ApitopkenizarTarjeta');
Route::resource('Tokenizador', 'TokenController');



Route::delete('Tokenizador/EliminarTarjetaCliente', 'TokenController@destroy');
Route::resource('Tokenizador', 'TokenController');
