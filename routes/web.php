<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('main');




Auth::routes();


Route::post('/CambioPass', 'Auth\ResetPasswordController@CambiarPass')->name('CambioPass');


Route::get('/Inicio', 'Auth\LoginController@Inicio')->name('Inicio');


Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {

	Route::get('/userregister', function () {

		$rols = DB::table('rol')->pluck('nombre', 'id');

		return view('pages.user_register',compact("rols"));
	})->name('show-userregister');
	

	Route::post('/createkey', 'LlaveController@store')->name('createkey');
	
	Route::get('/listarllave','LlaveController@index' )->name('listarllave');
	Route::get('getidlllave', 'LlaveController@GetIdLlave')->name('getidlllave');
	Route::get('/getidparameter', 'LlaveController@GetParamterKey')->name('getidparameter');
	Route::get('/updateestadollave', 'LlaveController@UpdateEstadoLlave')->name('updateestadollave');

	Route::post('updatellave', 'LlaveController@Update')->name('updatellave');
	Route::post('/userregister', 'Auth\RegisterController@register')->name('userregister');
	
	Route::get('crearToken', 'TokenController@index')->name('crearToken');

	Route::get('cargarTokens', 'TokenController@cargarTokens')->name('cargarTokens');

	Route::get('listarTokens', 'TokenController@listarToken')->name('listar');

	Route::get('eliminarToken', 'TokenController@eliminarToken')->name('eliminarToken');

	Route::put('editartoken', 'TokenController@editarToken')->name('editarToken');

	Route::get('BuscarToken', 'TokenController@buscarToken')->name('BuscarToken');

	Route::get('creditCardReveal', 'TokenController@creditCardReveal')->name('creditCardReveal');

	Route::get('foundComercio', 'TokenController@foundComercio')->name('foundComercio');

	Route::post('crearToken', 'TokenController@store')->name('crearToken');

	Route::get('cargarToken', function () {
		return view('pages.cargarToken');
	})->name('cargarToken');

	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

// Route::get('/{cadena}', function ($cadena) {

// 	$ValidarUrl =  DB::table('urlclientes')->where('url',$cadena)->first();
//     if (!is_null($ValidarUrl)) {
// 		$valorcomprobacion = 1;
// 		return view('welcome', compact("valorcomprobacion"));
// 	}else{
	
// 		return \Response::view('errors.404',array(),404);
// 	}  


//});
