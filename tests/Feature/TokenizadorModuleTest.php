<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;
class TokenizadorModule extends TestCase
{
    use RefreshDatabase;
    /**
     *
     * @return void
     */
    public function test_Crear_Tokenizador()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $this->seed();
        $response = $this->call('POST', '/crearToken', [
            'numero_tarjeta'=>'5125796296825872', //input
            'fecha_exp'=>'2020-16',
            'franchise'=>'VISA',
            'nombre_tarjeta'=>'pruebaText',
            'cvv'=>'256',
            'valor_pago'=>'25555',
            'referencia'=>'A02',
            'suscripcion'=>2,
            'busqueda'=>1,
            'fecha_pagos'=>'2020-05-11',
            'nombre_cliente'=>'clientePrueba',
            'apellido1_cliente'=>'apellido1Prueba',
            'apellido2_cliente'=>'apellido2Prueba',
            'email_cliente'=>'email@prueba.com',
            'cedula_cliente'=>'123456789',
            'telefono_cliente'=>'3172989958'
        ]);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('tarjeta', [
            'nombre'=>'pruebaText',
        ]);
    }
     /**
     * @return void
     */
    public function test_load_listarTokens()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $this->get('/listarTokens')
        ->assertSee('listarTokens');
    }
    /**
     * @return void
     */
    public function test_load_buscarTokens()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $this->get('/listarTokens')
        ->assertSee('listarTokens');
    }
     /**
     * @return void
     */
    public function test_load_crear()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $this->get('/crearToken')
        ->assertSee('crearToken');
    }
      /**
     * @return void
     */
    public function test_load_cargar()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $this->get('/cargarTokens')
        ->assertSee('cargarTokens');
    }
     /**
     * @return void
     */
    public function test_buscar_token()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->call('GET', '/BuscarToken', [
            'text'=>'tar', //input
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
