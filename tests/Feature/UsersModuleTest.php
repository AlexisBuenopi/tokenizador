<?php

namespace Tests\Feature;

use App\CodigoSeguridad;
use App\Mail\codeMail;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class UsersModuleTest extends TestCase
{

    use RefreshDatabase;
    /** @test */
    public function test_load_pages_login()
    {
        $this->get('/login')
             ->assertStatus(200)
             ->assertSee('login');
    }
    /** @test */
    public function test_load_pages_profile()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user)
             ->get('profile')
             ->assertStatus(200)
             ->assertSee('profile');
    }
    /** @test*/
    public function login_authenticates()
    {
        Mail::fake();
        $user = factory(User::class)->create();
        Session::start();

        $response = $this->call('POST', '/login', [
            'email' => $user->email,
            'password' => 'miprueba',
            '_token' => csrf_token(),
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        Mail::assertSent(codeMail::class, function ($mail) use ($user) {
            print_r($mail->msg);
            return $mail->to[0]['address'] == $user->email;
        });
        $this->assertEquals('auth.login', $response->original->name());
    }

    /** @test */
    public function test_login_authenticates_code()
    {
        $user = factory(User::class)->create();
        Session::start();

        //guardar codigo en base de datos y asignarle un estado
        $codigo = new CodigoSeguridad();
        $codigo->estado = 1; //activo
        $codigo->code = '12345';
        $codigo->save();
        User::where('id', $user->id)->update(['codigo_seguridad_id' => $codigo->id]);
        $response = $this->call('POST', '/login', [
            'email' => $user->email,
            'password' => 'miprueba',
            'code' => 12345,
            '_token' => csrf_token(),
        ]);

        $this->assertEquals(302, $response->getStatusCode());
        $response->assertRedirect(route('home'));
    }
    /** @test */
    public function test_login_bad_authenticates()
    {
        $user = factory(User::class)->create();
        Session::start();
        $response = $this->call('POST', '/login', [
            'email' => $user->email,
            'password' => 'miprueba2',
            '_token' => csrf_token(),
        ]);

        $this->assertEquals(302, $response->getStatusCode());
    }
    /*@test */
    public function test_register_admin()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->call('POST', '/userregister', [
            'name' => 'Valentina',
            'cargo' => 'admin',
            'email' => 'vsanto@uqvirtual.edu.co',
            'password' => 'hola',
            'password_confirmation' => 'hola',
        ]);

        $this->assertEquals(302, $response->getStatusCode());
    }

      /*@test */
      public function test_register_bad_admin()
      {
          $user = factory(User::class)->create();
          $this->actingAs($user);
  
          $response = $this->call('POST', '/userregister', [
              'name' => 'Valentina',
              'cargo' => 'admin',
              'email' => 'vsanto@uqvirtual.edu.co',
              'password' => 'hola',
              'password_confirmation' => '7816',
          ]);
  
          $this->assertEquals(302, $response->getStatusCode());
      }
}
